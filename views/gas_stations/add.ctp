<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('GasStation', array(
        'enctype' => 'multipart/form-data'
      ));
?>
	<fieldset>
 		<legend>Tambah SPBU</legend>
        <table class="input">
            <tr>
                <td class="label-required">Kecamatan<br />
                <span class="label">Nama kecamatan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('district_id', $districts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kelurahan<br />
                <span class="label">Nama kelurahan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('subdistrict_id', $subdistricts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('subdistrict_id')) ? $form->error('subdistrict_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jalan<br />
                <span class="label">Nama jalan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">No SPBU<br />
                <span class="label">No SPBU</span>
                </td>
                <td><?php echo $form->input('no', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Alamat lengkap<br />
                <span class="label">Alamat lengkap</span>
                </td>
                <td>
                <?php
                    echo $form->input('address', array(
                        'type' => 'textarea', 'div'=>false, 'label'=>false, 'class'=>'required',
                        'rows' => 2
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td>Telp. 1</td>
                <td>
                <?php
                    echo $form->input('phone_1', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td>Telp. 2</td>
                <td>
                <?php
                    echo $form->input('phone_2', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td>Fax</td>
                <td>
                <?php
                    echo $form->input('fax', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[GasStation][GasStationImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
<script type="text/javascript">
$(function() {
    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);
        
        e.preventDefault();
        return false;
    });
});
</script>
