<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php $html->script('jquery.carouFredSel-3.2.0', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
    <?php if (!empty($gas_station['GasStationImage']) ):?>
    .carousel {
        margin-bottom: 5px;
        width: 208px;
    }
    .carousel ul {
        background-color: #efefef;
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    .carousel li {
        color: #666;
        text-align: center;
        width: 30px;
        height: 48px;
        padding: 0;
        margin: 6px;
        display: block;
        float: left;
    }
    .carousel li a {
        display: block;
    }
    .carousel li a img {
        border: 2px solid #333;
    }
    .carousel li .delImage {
        font-size: 9px;
    }
    .clearfix {
        float: none;
        clear: both;
    }
    #prev {
        margin-left: 10px;
        font-weight: bold;
    }
    #next {
        float: right;
        margin-right: 10px;
        font-weight: bold;
    }
    .carousel .disabled
    {
        font-weight: normal !important;
        text-decoration: none;
        color: #999;
        cursor: default;
    }
    <?php endif;?>
</style>

<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('GasStation', array(
        'enctype' => 'multipart/form-data'
      ));
?>
	<fieldset>
 		<legend>Edit SPBU</legend>
        <table class="input">
            <tr>
                <td class="label-required">Kecamatan<br />
                <span class="label">Nama kecamatan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('district_id', $districts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kelurahan<br />
                <span class="label">Nama kelurahan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('subdistrict_id', $subdistricts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('subdistrict_id')) ? $form->error('subdistrict_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jalan<br />
                <span class="label">Nama jalan<br />dimana SPBU berada</span>
                </td>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">No SPBU<br />
                <span class="label">No SPBU</span>
                </td>
                <td><?php echo $form->input('no', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Alamat lengkap<br />
                <span class="label">Alamat lengkap</span>
                </td>
                <td>
                <?php
                    echo $form->input('address', array(
                        'type' => 'textarea', 'div'=>false, 'label'=>false, 'class'=>'required',
                        'rows' => 2
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td>Telp. 1</td>
                <td>
                <?php
                    echo $form->input('phone_1', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td>Telp. 2</td>
                <td>
                <?php
                    echo $form->input('phone_2', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td>Fax</td>
                <td>
                <?php
                    echo $form->input('fax', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div class="carousel">
                <?php
                    if (!empty($gas_station['GasStationImage']) ):
                        echo '<ul>';
                        foreach ( $gas_station['GasStationImage'] as $cimg):
                            echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                        '" class="colorbox" rel="' . $gas_station['GasStation']['id'] . '">' .
                                 '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                 'width="25" height="25" /></a>' .
                                 $html->link('Hapus', array('action' => 'delImage', $cimg['id'], $gas_station['GasStation']['id']),
                                    array('class' => 'delImage')
                                 ) . '</li>';
                        endforeach;
                        echo '</ul>';
                        echo '<div class="clearfix"></div>';
                        echo '<a id="prev" href="#">&lt;</a>';
                        echo '<a id="next" href="#">&gt;</a>';
                    endif;
                ?>
                </div>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[GasStation][GasStationImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span><br />
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
<script type="text/javascript">
$(function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $gas_station['GasStation']['no'];?></h3>" +
               "<br /><?php echo $gas_station['GasStation']['address'];?>" +
               '<br /><?php echo $this->Html->link('Hapus', array('action' => 'unsetLatLng', $gas_station['GasStation']['id']), array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $gas_station['GasStation']['id'];?>";
    
    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);
        
        e.preventDefault();
        return false;
    });
    
    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });
    
    $('.colorbox').colorbox({
        close: "Tutup"
    });

    <?php if (!empty($gas_station['GasStationImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
    
    function init() {
    <?php if ( !empty($gas_station['GasStation']['lat']) && !empty($gas_station['GasStation']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $gas_station['GasStation']['lat'] . ', ' . $gas_station['GasStation']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($gas_station['GasStation']['lat']) && !empty($gas_station['GasStation']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $gas_station['GasStation']['no'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
