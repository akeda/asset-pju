<?php echo $this->element('excelfilter',
        array(
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Jalan',
                        'options' => $streets
                    ),
                    'relay_id' => array(
                        'label' => 'Gardu',
                        'options' => $relays
                    ),
                    'panel_id' => array(
                        'label' => 'Panel',
                        'options' => $panels
                    )
                ),
                array(
                    'network_size_id' => array(
                        'label' => 'Ukuran jaringan',
                        'options' => $network_sizes
                    ),
                    'lamp_type_id' => array(
                        'label' => 'Tipe Lampu',
                        'options' => $types
                    ),
                    'name' => 'Kode lampu'
                )
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Print excel data lampu'
        ));
?>
