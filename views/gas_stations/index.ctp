<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "no" => array(
                    'title' => 'No SPBU',
                    'sortable' => true
                ),
                "district_id" => array(
                    'title' => 'Kecamatan',
                    'sortable' => false
                ),
                "subdistrict_id" => array(
                    'title' => 'Kelurahan',
                    'sortable' => false
                ),
                'address' => array(
                    'title' => 'Alamat',
                    'sortable' => false
                ),
                'actionLinks' => array(
                    'title' => '',
                    'sortable' => false
                )
            ),
            'filter' => array(
                array(
                    'district_id' => array(
                        'label' => 'Kecamatan',
                        'options' => $districts
                    ),
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Jalan',
                        'options' => $streets
                    )
                ),
                'name' => 'No SPBU'
            ),
            'filter_max_col' => 3,
            'filter_title' => 'Pencarian data SPBU',
            "editable"  => "no",
            "assoc" => array(
                "district_id" => array(
                    'model' => 'District',
                    'field' => 'name'
                ),
                "subdistrict_id" => array(
                    'model' => 'Subdistrict',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
<script type="text/javascript">
var cb;
$(function() {
    cb = $('.colorbox').colorbox({
        close: "Tutup"
    });
});
</script>
