<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => __('Nama Jalan', true),
                    'sortable' => true
                ),
                'subdistrict_id' => array(
                    'title' => __('Kelurahan', true),
                    'sortable' => true
                ),
                'district_id' => array(
                    'title' => __('Kecamatan', true),
                    'sortable' => false
                ),
                "municipality_id" => array(
                    'title' => __('Kotamadya / Kabupaten', true),
                    'sortable' => false
                ),
                "province_id" => array(
                    'title' => __('Propinsi', true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                )
            ),
            "editable"  => "name",
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_type_id' => array(
                        'label' => 'Tipe jalan',
                        'options' => $street_types
                    )
                ),
                'name' => 'Nama jalan'
            ),
            'filter_max_col' => 2,
            'filter_title' => 'Pencarian nama jalan',
            "assoc" => array(
                'subdistrict_id' => array(
                    'model' => 'Subdistrict',
                    'field' => 'name'
                ),
                "district_id" => array(
                    'model' => 'District',
                    'field' => 'name'
                ),
                "municipality_id" => array(
                    'model' => 'Municipality',
                    'field' => 'name'
                ),
                "province_id" => array(
                    'model' => 'Province',
                    'field' => 'name'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
            ),
            "displayedAs" => array(
                'created' => 'datetime'
            )
        ));
?>
</div>
