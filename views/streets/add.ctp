<?php echo $javascript->codeBlock($ajaxURL);?>
<?php echo $javascript->link('streets', false);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Street');?>
	<fieldset>
 		<legend>Tambah Jalan</legend>
        <ul class="steps">
            <li class="first"><a href="#"><strong>1. Pilih propinsi</strong></a></li>
            <li><a href="#"><strong>2. Pilih Kotamadya / kabupaten</strong></a></li>
            <li><a href="#"><strong>3. Pilih kecamatan</strong></a></li>
            <li><a href="#"><strong>4. Pilih kelurahan</strong></a></li>
            <li><a href="#"><strong>5. Pilih tipe jalan</strong></a></li>
            <li><a href="#"><strong>6. Nama jalan</strong></a></li>
        </ul>
        <br class="clear" />
        <table class="input">
            <tr>
                <td class="label-required">Propinsi</td>
                <td>
                <?php 
                    echo $form->select('province_id', $provinces, $province_id, array(
                        'div' => false, 'label'=>false, 'class'=>'required ajax_select select',
                        'rel' => '#StreetMunicipalityId', 'ref' => 'province_id', 'empty' => ''
                        ), true);
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kotamadya / Kabupaten</td>
                <td>
                <?php 
                    echo $form->select('municipality_id', $municipalities, $municipality_id, array(
                        'div' => false, 'label' => false, 'class' => 'required ajax_select select',
                        'model' => 'Municipality', 'field' => 'name',
                        'rel' => '#StreetDistrictId', 'ref' => 'municipality_id'
                        ), true);
                    echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kecamatan</td>
                <td>
                <?php 
                    echo $form->select('district_id', $districts, $district_id, array(
                        'div'=>false, 'label'=>false, 'class'=>'required ajax_select select',
                        'model' => 'District', 'field' => 'name',
                        'rel' => '#StreetSubdistrictId', 'ref' => 'district_id'
                        ), true);
                    echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kelurahan</td>
                <td>
                <?php 
                    echo $form->select('subdistrict_id', $subdistricts, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required ajax_select select',
                        'model' => 'Subdistrict', 'field' => 'name'
                        ), true);
                    echo ($form->isFieldError('subdistrict_id')) ? $form->error('subdistrict_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tipe Jalan</td>
                <td>
                <?php 
                    echo $form->select('street_type_id', $street_types, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required select'
                        ), true);
                    echo ($form->isFieldError('street_type_id')) ? $form->error('street_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama Jalan</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label'=>false, 'class'=>'required inputText'
                    ));
                ?>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
