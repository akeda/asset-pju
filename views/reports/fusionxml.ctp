<graph xAxisName="Car series" yAxisName="Qty" decimalPrecision="0" formatNumberScale="0">
<?php foreach ($records as $record):?>
<?php
    echo '<set name="Series ' . $record['name'] . '" value="' . $record['qty'] . '" color="AFD8F8"/>';
?>
<?php endforeach;?>
</graph>
