<style type="text/css">
.numeric { text-align: right !important; }
</style>
<?php echo $html->script('jquery.colorbox-min.js', array('inline' => false));?>
<?php echo $html->css('colorbox.css', 'stylesheet', array('inline' => false));?>
<div class="<?php echo $this->params['controller']; ?> index">
<!-- begin tablegrid-head -->
<div class="tablegrid-head">
    <div class="module-head">
        <div class="module-head-c">
            <h2>Chart lampu berdasar tipe</h2>
        </div>
    </div>
    <br class="clear" />
    <?php
        echo $this->element('chartfilter', array(
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Jalan',
                        'options' => $streets
                    ),
                    'relay_id' => array(
                        'label' => 'Gardu',
                        'options' => $relays
                    ),
                    'panel_id' => array(
                        'label' => 'Panel',
                        'options' => $panels
                    )
                ),
                array(
                    'network_type_id' => array(
                        'label' => 'Tipe Jaringan',
                        'options' => $network_types
                    ),
                    'network_size_id' => array(
                        'label' => 'Ukuran Jaringan',
                        'options' => $network_sizes
                    ),
                    'lamp_type_id' => array(
                        'label' => 'Tipe Lampu',
                        'options' => $lamp_types
                    )
                )
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Filter chart'
        ));
    ?>
</div>

<?php echo $html->script('FusionCharts', array('inline' => false));?>
<div id="flashcontent" style="position: relative; z-index: 0">
    <strong>You need to upgrade your Flash Player</strong>
</div>
<script type="text/javascript">
var chart = new FusionCharts("<?php echo $fusionURL;?>FCF_Column3D.swf", "ChartId_<?php echo md5(mktime());?>", "600", "350");
chart.setDataURL("<?php echo $controllerURL;?>/fusionxml/<?php echo urlencode($str_fl);?>");
chart.setTransparent(1);
chart.render("flashcontent");
</script>

<table cellpadding="0" cellspacing="0" class="tablegrid" id="tablegrid_<?php echo $this->params['controller'];?>">
<thead>
    <tr>
        <th>No. </th>
        <th>Tipe Lampu</th>
        <th>Jumlah</th>
        <th>&nbsp;</th>
    </tr>
</thead>
<?php if (!empty($records)):?>
<?php
    $no = 1; $total = 0; $i = 0;
?>
<tbody>
    <?php foreach ($records as $record):?>
    <?php
        $class = null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
    ?>
    <tr<?php echo $class;?>>
        <td>
            <?php echo $no++;?>
        </td>
        <td>
        <?php
            echo $record['name'];
        ?>
        </td>
        <td class="numeric">
            <?php echo $record['qty'];?>
        </td>
        <td>
        <?php
            echo $html->link('See detail', array(
                'action' => 'viewdetail', urlencode('?search=Filter&' . $str_fl_excel . '&opt_lamp_type_id=' . 
                $record['id'])
            ), array(
                'class' => 'colorbox', 'title' => $record['name']
            )) . ' &nbsp; | &nbsp; ';
            echo $html->link('Detail in excel', array(
                'controller' => 'lamps', 'action' => 'excel', urlencode('?search=Print&' . $str_fl_excel . '&opt_lamp_type_id=' . 
                $record['id']))
            );
            
            $total += $record['qty'];
        ?>
        </td>
    </tr>
    <?php endforeach;?>
    <tfoot>
        <tr>
        <th colspan="2">Total</th>
        <th class="numeric"><?php echo $total;?></th>
        <th>
        
        </th>
        </tr>
    </tfoot>
</tbody>
<?php endif;?>
</table>
</div>
<script type="text/javascript">
$(function() {
    $('.colorbox').colorbox();
});
</script>
