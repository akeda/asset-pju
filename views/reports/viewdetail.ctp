<?php echo $html->script('jquery.tablesorter.min', array('inline' => true));?>
<style type="text/css">
.tablegrid {
    margin-bottom: 0;
}
</style>
<table cellpadding="0" cellspacing="0" class="tablegrid" id="tablegrid_<?php echo $this->params['controller'];?>">
<thead>
    <tr>
        <th>No. </th>
        <th>Kode lampu</th>
        <th>Jenis lampu</th>
        <th>Jenis Tiang</th>
        <th>Ukuran jaringan</th>
        <th>Jenis jaringan</th>
        <th>Jalan</th>
        <th>Gardu</th>
        <th>Panel</th>
        <th>Catatan</th>
    </tr>
</thead>
<?php if (!empty($records)):?>
<?php
    $no = 1;
?>
<tbody>
    <?php foreach ($records as $record):?>
    <tr>
        <td>
            <?php echo $no++;?>
        </td>
        <td><?php echo $record['Lamp']['name'];?></td>
        <td><?php echo $record['LampType']['name'];?></td>
        <td><?php echo $record['PillarType']['name'];?></td>
        <td><?php echo $record['NetworkSize']['name'];;?></td>
        <td><?php echo $record['NetworkType']['name'];?></td>
        <td><?php echo $record['Street']['name'];?></td>
        <td><?php echo $record['Relay']['name'];?></td>
        <td><?php echo $record['Panel']['name'];?></td>
        <td><?php echo $record['Lamp']['note'];?></td>
    </tr>
    <?php endforeach;?>
</tbody>
<?php endif;?>
</table>
<script type="text/javascript">
$(function() {
    $('#tablegrid_<?php echo $this->params['controller'];?>').tablesorter();
});
</script>

