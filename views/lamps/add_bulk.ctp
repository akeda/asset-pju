<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Lamp');?>
	<fieldset>
 		<legend>Tambah Lampu</legend>
        <table class="input">
            <tr>
                <td class="label-required">Kelurahan<br />
                <span class="label">Nama kelurahan<br />dimana lampu terpasang
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('subdistrict_id', $subdistricts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('subdistrict_id')) ? $form->error('subdistrict_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jalan<br />
                <span class="label">Nama jalan<br />dimana lampu terpasang
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gardu<br />
                <span class="label">Sumber gardu ke-<br /> panel dimana lampu terhubung
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('relay_id', $relays, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('relay_id')) ? $form->error('relay_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Panel<br />
                <span class="label">Sumber panel ke lampu
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('panel_id', $panels, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('panel_id')) ? $form->error('panel_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tipe Lampu<br />
                <span class="label">Tipe lampu
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('lamp_type_id', $types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('lamp_type_id')) ? $form->error('lamp_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jenis Tiang<br />
                <span class="label">Jenis tiang<br />dimana lampu terpasang</span>
                </td>
                <td>
                <?php 
                    echo $form->select('pillar_type_id', $pillar_types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('pillar_type_id')) ? $form->error('pillar_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Ukuran jaringan</td>
                <td>
                <?php 
                    echo $form->select('network_size_id', $network_sizes, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('network_size_id')) ? $form->error('network_size_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jenis jaringan</td>
                <td>
                <?php 
                    echo $form->select('network_type_id', $network_types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('network_type_id')) ? $form->error('network_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Catatan<br />
                <span class="label">Catatan
                </span>
                </td>
                <td>
                <?php
                    echo $form->input('note', array('div'=>false, 'label'=>false, 'type' => 'textarea'));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jumlah lampu<br />
                <span class="label">Jumlah lampu
                </span>
                </td>
                <td>
                <?php
                    echo $form->input('qty', array('div'=>false, 'label'=>false));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
