<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Lamp');?>
	<fieldset>
 		<legend>Tambah Lampu</legend>
        <table class="input">
            <tr>
                <td class="label-required">Kelurahan<br />
                <span class="label">Nama kelurahan<br />dimana lampu terpasang</span>
                </td>
                <td>
                <?php 
                    echo $form->select('subdistrict_id', $subdistricts, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('subdistrict_id')) ? $form->error('subdistrict_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jalan<br />
                <span class="label">Nama jalan<br />dimana lampu terpasang</span>
                </td>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gardu<br />
                <span class="label">Sumber gardu ke-<br /> panel dimana lampu terhubung</span>
                </td>
                <td>
                <?php 
                    echo $form->select('relay_id', $relays, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('relay_id')) ? $form->error('relay_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Panel<br />
                <span class="label">Sumber panel ke lampu
                </span>
                <td>
                <?php 
                    echo $form->select('panel_id', $panels, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('panel_id')) ? $form->error('panel_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tipe Lampu<br />
                <span class="label">Tipe lampu
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('lamp_type_id', $types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('lamp_type_id')) ? $form->error('lamp_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jenis Tiang<br />
                <span class="label">Jenis tiang<br />dimana lampu terpasang</span>
                </td>
                <td>
                <?php 
                    echo $form->select('pillar_type_id', $pillar_types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('pillar_type_id')) ? $form->error('pillar_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Ukuran jaringan</td>
                <td>
                <?php 
                    echo $form->select('network_size_id', $network_sizes, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('network_size_id')) ? $form->error('network_size_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jenis jaringan</td>
                <td>
                <?php 
                    echo $form->select('network_type_id', $network_types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('network_type_id')) ? $form->error('network_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama lampu<br />
                <span class="label">Nama/kode lampu</span>
                </td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Koordinat<br />
                <span class="label">Koordinat GPS</span>
                </td>
                <td>
                <?php
                    echo $form->input('coordinate', array(
                        'type' => 'textarea', 'div'=>false, 'label'=>false, 'class'=>'required',
                        'rows' => 2
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Catatan<br />
                <span class="label">Catatan
                </span>
                </td>
                <td>
                <?php
                    echo $form->input('note', array('div'=>false, 'label'=>false, 'type' => 'textarea'));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Peta<br />
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span>
                </td>
                <td>
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Lamp']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Lamp']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
$(function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $lamp['Lamp']['name'];?>, tipe <?php echo $lamp['LampType']['name'];?></h3>" +
               "<br />Gardu <?php echo $lamp['Relay']['name'];?>, panel <?php echo $lamp['Panel']['name'];?>" +
               "<br /><?php echo $lamp['Street']['name'];?>" +
               '<br /><?php echo $this->Html->link('Hapus', array('action' => 'unsetLatLng', $lamp['Lamp']['id']), array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $lamp['Lamp']['id'];?>";
    function init() {
    <?php if ( !empty($lamp['Lamp']['lat']) && !empty($lamp['Lamp']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $lamp['Lamp']['lat'] . ', ' . $lamp['Lamp']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($lamp['Lamp']['lat']) && !empty($lamp['Lamp']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $lamp['Lamp']['name'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
