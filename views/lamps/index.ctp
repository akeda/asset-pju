<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => 'Kode Lampu',
                    'sortable' => true
                ),
                "coordinate" => array(
                    'title' => 'Koordinat',
                    'sortable' => true
                ),
                'lamp_type_id' => array(
                    'title' => 'Jenis Lampu',
                    'sortable' => true
                ),
                'pillar_type_id' => array(
                    'title' => 'Jenis Tiang',
                    'sortable' => true
                ),
                'network_size_id' => array(
                    'title' => 'Ukuran Jaringan',
                    'sortable' => true
                ),
                'network_type_id' => array(
                    'title' => 'Jenis Jaringan',
                    'sortable' => true
                ),
                "street_id" => array(
                    'title' => 'Jalan',
                    'sortable' => false
                ),
                "relay_id" => array(
                    'title' => 'Gardu',
                    'sortable' => false
                ),
                "panel_id" => array(
                    'title' => 'Panel',
                    'sortable' => false
                ),
                "note" => array(
                    'title' => 'Catatan',
                    'sortable' => false
                ),
                'actionLinks' => array(
                    'title' => '',
                    'sortable' => false
                )
            ),
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Jalan',
                        'options' => $streets
                    ),
                    'relay_id' => array(
                        'label' => 'Gardu',
                        'options' => $relays
                    ),
                    'panel_id' => array(
                        'label' => 'Panel',
                        'options' => $panels
                    )
                ),
                array(
                    'network_size_id' => array(
                        'label' => 'Ukuran jaringan',
                        'options' => $network_sizes
                    ),
                    'lamp_type_id' => array(
                        'label' => 'Tipe Lampu',
                        'options' => $types
                    ),
                    'name' => 'Kode lampu'
                )
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Pencarian data lampu',
            "editable"  => "name",
            "assoc" => array(
                "street_id" => array(
                    'model' => 'Street',
                    'field' => 'name'
                ),
                "relay_id" => array(
                    'model' => 'Relay',
                    'field' => 'name'
                ),
                "panel_id" => array(
                    'model' => 'Panel',
                    'field' => 'name'
                ),
                'lamp_type_id' => array(
                    'model' => 'LampType',
                    'field' => 'name'
                ),
                'pillar_type_id' => array(
                    'model' => 'PillarType',
                    'field' => 'name'
                ),
                'network_size_id' => array(
                    'model' => 'NetworkSize',
                    'field' => 'name'
                ),
                'network_type_id' => array(
                    'model' => 'NetworkType',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
<script type="text/javascript">
var cb;
$(function() {
    cb = $('.colorbox').colorbox({
        close: "Tutup"
    });
});
</script>
