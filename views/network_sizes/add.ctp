<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('NetworkSize');?>
	<fieldset>
 		<legend>Tambah Ukuran Jaringan</legend>
        <table class="input">
            <tr>
                <td class="label-required">Jenis jaringan</td>
                <td>
                <?php 
                    echo $form->select('network_type_id', $types, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                    echo ($form->isFieldError('network_type_id')) ? $form->error('network_type_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Ukuran</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
