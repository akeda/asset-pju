<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => 'Ukuran',
                    'sortable' => true
                ),
                "network_type_id" => array(
                    'title' => 'Jenis jaringan',
                    'sortable' => true
                )
            ),
            "editable"  => "name",
            "assoc" => array(
                "network_type_id" => array(
                    'model' => 'NetworkType',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
