<?php echo $javascript->codeBlock($ajaxURL);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Subdistrict');?>
	<fieldset>
 		<legend><?php __('Edit Kelurahan');?></legend>
        <ul class="steps">
            <li class="first"><a href="#"><strong>1. Pilih propinsi</strong></a></li>
            <li><a href="#"><strong>2. Pilih Kotamadya / kabupaten</strong></a></li>
            <li><a href="#"><strong>3. Pilih kecamatan</strong></a></li>
            <li><a href="#"><strong>4. Isi nama kelurahan</strong></a></li>
        </ul>
        <br class="clear" />
        <table class="input">
            <tr>
                <td class="label-required">Propinsi</td>
                <td>
                <?php 
                    echo $form->select('province_id', $provinces, $province_id, array(
                        'div' => false, 'label'=>false, 'class'=>'required ajax_select select',
                        'rel' => '#SubdistrictMunicipalityId', 'ref' => 'province_id', 'empty' => ''
                        ), true);
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kotamadya / Kabupaten</td>
                <td>
                <?php 
                    echo $form->select('municipality_id', $municipalities, $municipality_id, array(
                        'div' => false, 'label' => false, 'class' => 'required ajax_select select',
                        'model' => 'Municipality', 'field' => 'name',
                        'rel' => '#SubdistrictDistrictId', 'ref' => 'municipality_id'
                        ), true);
                    echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kecamatan</td>
                <td>
                <?php 
                    echo $form->select('district_id', $districts, $district_id, array(
                        'div'=>false, 'label'=>false, 'class'=>'required ajax_select select',
                        'model' => 'District', 'field' => 'name'
                        ), true);
                    echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Nama Kelurahan', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Code', true);?></td>
                <td><?php echo $form->input('code', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(
                        __('Delete', true),
                        array('action'=>'delete', $this->data['Subdistrict']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Subdistrict']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
