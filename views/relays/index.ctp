<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => __('Gardu', true),
                    'sortable' => true
                ),
                "street" => array(
                    'title' => 'Jalan yang dilayani',
                    'sortable' => false
                ),
                'coverage_service_id' => array(
                    'title' => 'Area PLN',
                    'sortable' => false
                ),
                'total_panels' => array(
                    'title' => 'Jumlah panel',
                    'sortable' => false
                ),
                'total_lamps' => array(
                    'title' => 'Jumlah existing',
                    'sortable' => false
                ),
                'actionLinks' => array(
                    'title' => '',
                    'sortable' => false
                )
            ),
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Nama jalan',
                        'options' => $streets
                    ),
                    'coverage_service_id' => array(
                        'label' => 'Area PLN',
                        'options' => $coverage_services
                    )
                ),
                'name' => 'Nama gardu'
            ),
            'filter_max_col' => 3,
            'filter_title' => 'Pencarian gardu',
            "editable"  => "name",
            'assoc' => array(
                'coverage_area_id' => array(
                    'model' => 'CoverageArea',
                    'field' => 'name'
                ),
                'coverage_service_id' => array(
                    'model' => 'CoverageService',
                    'field' => 'name'
                )
            )
        ));
?>
</div>
<script type="text/javascript">
var cb;
$(function() {
    cb = $('.colorbox').colorbox({
        close: "Tutup"
    });
});
</script>
