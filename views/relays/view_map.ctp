<div id="map_canvas"></div>
<script type="text/javascript">
$(document).bind('cbox_complete', function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $relay['Relay']['name'];?></h3>" +
               "<?php echo $streets_iw;?>";
    function init() {
    <?php if ( !empty($relay['Relay']['lat']) && !empty($relay['Relay']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $relay['Relay']['lat'] . ', ' . $relay['Relay']['lng'];?>);
    <?php else:?>
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
    <?php if ( !empty($relay['Relay']['lat']) && !empty($relay['Relay']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $relay['Relay']['name'];?>"
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
    }
    init();
});
</script>
