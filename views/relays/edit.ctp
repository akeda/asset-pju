<?php echo $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Relay', array(
        'enctype' => 'multipart/form-data'
    ));
?>
	<fieldset>
 		<legend>Edit Gardu</legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name', true);?><br />
                <span class="label">Masukkan nama/kode gardu PLN</span>
                </td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Jalan yang dilayani<br />
                <span class="label">Tekan Ctrl dan klik nama jalan<br />
                untuk pemilihan lebih dari satu.
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('Street', $streets, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required', 'multiple' => true
                        ), true);
                ?>
                </td>
            </tr>
            <!--
            <tr>
                <td class="label-required">Area Jaringan<br />
                <span class="label">Area Jaringan
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('coverage_area_id', $coverage_areas, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                ?>
                </td>
            </tr>
            -->
            <tr>
                <td class="label-required">Area PLN<br />
                <span class="label">Area PLN
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('coverage_service_id', $coverage_services, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <?php if ( !empty($this->data['Relay']['imagename']) ):
                    echo '<a href="' . $pathImage . $this->data['Relay']['imagename'] . '" class="colorbox" title="' . $this->data['Relay']['name'] . '"' .
                         '<img src="' . $pathImage . $resizedPrefix . $this->data['Relay']['imagename'] . '" /></a>';
                    echo '<br /><span class="label">Klik gambar untuk memperbesar</span>';
                    echo '<br />';
                endif;?>
                <?php
                    echo $form->input('imagename', array(
                        'div'=>false, 'label'=>false, 'class'=>'required',
                        'type' => 'file'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Peta<br />
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span>
                </td>
                <td>
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Relay']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Relay']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
$(function() {
    $('a.colorbox').colorbox();
    
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $relay['Relay']['name'];?></h3>" +
               "<?php echo $streets_iw;?>" +
               '<?php echo $this->Html->link('Hapus', array('action' => 'unsetLatLng', $relay['Relay']['id']), array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $relay['Relay']['id'];?>";
    function init() {
    <?php if ( !empty($relay['Relay']['lat']) && !empty($relay['Relay']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $relay['Relay']['lat'] . ', ' . $relay['Relay']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($relay['Relay']['lat']) && !empty($relay['Relay']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $relay['Relay']['name'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
