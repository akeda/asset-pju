<?php echo $this->element('excelfilter',
        array(
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Nama jalan',
                        'options' => $streets
                    ),
                    'coverage_area_id' => array(
                        'label' => 'Area jaringan',
                        'options' => $coverage_areas
                    ),
                    'coverage_service_id' => array(
                        'label' => 'Area pelayanan',
                        'options' => $coverage_services
                    )
                ),
                'name' => 'Nama gardu'
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Print excel gardu',
        ));
?>
