<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Relay', array(
        'enctype' => 'multipart/form-data'
    ));
?>
	<fieldset>
 		<legend>Tambah Gardu</legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name', true);?><br />
                <span class="label">Masukkan nama/kode gardu PLN</span>
                </td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Jalan yang dilayani<br />
                <span class="label">Tekan Ctrl dan klik nama jalan<br />
                untuk pemilihan lebih dari satu.
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('Street', $streets, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required', 'multiple' => true
                        ), true);
                ?>
                </td>
            </tr>
            <!--
            <tr>
                <td class="label-required">Area Jaringan<br />
                <span class="label">Area Jaringan
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('coverage_area_id', $coverage_areas, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                ?>
                </td>
            </tr>
            -->
            <tr>
                <td class="label-required">Area PLN<br />
                <span class="label">Area PLN
                </span>
                </td>
                <td>
                <?php 
                    echo $form->select('coverage_service_id', $coverage_services, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <?php
                    echo $form->input('imagename', array(
                        'div'=>false, 'label'=>false, 'class'=>'required',
                        'type' => 'file'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
