<?php echo $javascript->codeBlock($ajaxURL);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('CompanyReport');
?>
	<fieldset>
 		<legend>Edit pelaporan</legend>
        <table class="input">
            <tr>
                <td colspan="2">Tanggal pelaporan</td>
            </tr>
            <tr>    
                <td colspan="2">
                <?php
                    echo $form->input('report_date', array(
                        'div' => false, 'label' => false,
                        'type' => 'date', 'dateFormat' => 'DMY',
                        'maxYear' => date('Y'), 'minYear' => 2000
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="label">
                    Update data yang sekiranya berubah pada pelaporan saja, lalu klik tombol update
                    di kanan bawah
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            
            <tr>
                <!-- left -->
                <td>
                <table class="input">
                    <tr>
                        <td class="label-required">Propinsi</td>
                        <td>
                        <?php 
                            echo $form->select('province_id', $provinces, $province_id, array(
                                'div' => false, 'label'=>false, 'class'=>'required ajax_select select',
                                'rel' => '#CompanyMunicipalityId', 'ref' => 'province_id', 'empty' => ''
                                ), true);
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kotamadya / Kabupaten</td>
                        <td>
                        <?php 
                            echo $form->select('municipality_id', $municipalities, $municipality_id, array(
                                'div' => false, 'label' => false, 'class' => 'required ajax_select select',
                                'model' => 'Municipality', 'field' => 'name',
                                'rel' => '#CompanyDistrictId', 'ref' => 'municipality_id'
                                ), true);
                            echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kecamatan</td>
                        <td>
                        <?php 
                            echo $form->select('district_id', $districts, $district_id, array(
                                'div'=>false, 'label'=>false, 'class'=>'required ajax_select select',
                                'model' => 'District', 'field' => 'name'
                                ), true);
                            echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Nama perusahaan</td>
                        <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Jenis Perusahaan</td>
                        <td>
                            <?php
                            echo $form->select('company_type', $company_types, null, null, true);
                            echo ($form->isFieldError('company_type')) ? $form->error('company_type') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">NPWP</td>
                        <td><?php echo $form->input('npwp', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Alamat</td>
                        <td>
                        <?php
                            echo $form->input('address', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 3
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Contact Person</td>
                        <td>
                        <?php
                            echo $form->input('contact_person', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 2
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Telp. 1</td>
                        <td>
                        <?php
                            echo $form->input('phone_1', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Telp. 2</td>
                        <td>
                        <?php
                            echo $form->input('phone_2', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Fax</td>
                        <td>
                        <?php
                            echo $form->input('fax', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">&nbsp;</td>
                        <td><span class="label">Untuk update gambar<br />
                        dan peta ada<br />
                        di menu data industri</span><br />
                        <?php
                            echo $html->link('Klik disini', array(
                                'controller' => 'companies', 'action' => 'edit', $this->data['Company']['id'], '#upload_image'
                            ));
                        ?>
                        </td>
                    </tr>
                </table>
                </td>
                <!-- right -->
                <td style="padding-left: 20px;">
                <table class="input">
                    <tr>
                        <td class="label-required">No. TDI/IUI</td>
                        <td>
                        <?php
                            echo $form->input('registration_no', array(
                                'div'=>false, 'label'=>false, 'class'=>'required'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Tgl. TDI/IUI</td>
                        <td>
                        <?php
                            echo $form->input('registration_date', array(
                                'div'=>false, 'label'=>false, 'class'=>'required', 'type' => 'date',
                                'dateFormat' => 'DMY', 'maxYear' => date('Y'), 'minYear' => 2000
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kategori industri</td>
                        <td>
                            <?php
                            echo $form->select('industry_category', $industry_categories, null, null, true);
                            echo ($form->isFieldError('industry_category')) ? $form->error('industry_category') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">KBLI<br />
                        <span class="label">Klasifikasi Baku <br />Lapangan Usaha Indonesia</span>
                        </td>
                        <td><?php echo $form->input('kbli', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Kategori Unit</td>
                        <td>
                            <?php
                            echo $form->select('unit_category', $unit_categories, null, null, true);
                            echo ($form->isFieldError('unit_category')) ? $form->error('unit_category') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Jenis Usaha</td>
                        <td>
                        <?php
                            echo $form->input('activity_type', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 3
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Komoditi</td>
                        <td>
                        <?php
                            echo $form->input('commodity', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 2
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">KKI<br />
                        <span class="label">Klasifikasi Komoditi Industri</span>
                        </td>
                        <td>
                        <?php
                            echo $form->input('kki', array(
                                'div' => false, 'label' => false, 'class' => 'required'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kapasitas</td>
                        <td>
                        <?php
                            echo $form->input('commodity_capacity', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('commodity_capacity_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Satuan</span>'
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Total investasi</td>
                        <td>
                        <?php
                            echo $form->input('total_invest', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Tenaga kerja</td>
                        <td>
                        <?php
                            echo $form->input('resource_male', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; <span class="label">Laki-laki</span><br />';
                            echo '<div>&nbsp;</div>';
                            echo $form->input('resource_female', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; <span class="label">Perempuan</span>';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Limbah</td>
                        <td>
                        <?php
                            echo $form->input('solid_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('solid_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Padat</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('liquid_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('liquid_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Cair</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('gas_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('gas_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Gas</span>';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Pemakaian Sumber Daya</td>
                        <td>
                        <?php
                            echo $form->input('power_usage', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('power_usage_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Listrik</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('water_usage', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('water_usage_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Air</span><br />';
                            echo '<div>&nbsp;</div>';
                        ?>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Update', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['CompanyReport']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['CompanyReport']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
