<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('District');?>
	<fieldset>
 		<legend><?php __('Tambah Kecamatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Kotamadya / Kabupaten', true);?></td>
                <td>
                <?php 
                    echo $form->select('municipality_id', $municipalities, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                    echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Nama Kelurahan', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Code', true);?></td>
                <td><?php echo $form->input('code', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
