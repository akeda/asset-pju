<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => __('Kecamatan', true),
                    'sortable' => true
                ),
                "code" => array(
                    'title' => __('Code', true),
                    'sortable' => true
                ),
                "municipality_id" => array(
                    'title' => __('Kotamadya / Kabupaten', true),
                    'sortable' => true
                ),
                "province_id" => array(
                    'title' => __('Propinsi', true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                )
            ),
            "editable"  => "name",
            "assoc" => array(
                "municipality_id" => array(
                    'model' => 'Municipality',
                    'field' => 'name'
                ),
                "province_id" => array(
                    'model' => 'Province',
                    'field' => 'name'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
            ),
            "displayedAs" => array(
                'created' => 'datetime'
            )
        ));
?>
</div>
