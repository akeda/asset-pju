<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('District');?>
	<fieldset>
 		<legend><?php __('Edit Kecamatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Kotamadya / Kabupaten', true);?></td>
                <td>
                <?php 
                    echo $form->select('municipality_id', $municipalities, null, array(
                        'div'=>false, 'label'=>false, 'class'=>'required'
                        ), true);
                    echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Nama Kelurahan', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Code', true);?></td>
                <td><?php echo $form->input('code', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(
                        __('Delete', true),
                        array('action'=>'delete', $this->data['District']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['District']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
