<?php $html->css('orbit', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.orbit', array('inline' => false));?>

<style type="text/css">

</style>
<h1>Selamat datang di Aplikasi Sudin Perindustrian &amp; Energi Kota Administrasi Jakarta Timur</h1>

<div id="featured" style="width: 940px; height: 600px; margin-top: 20px;">
    <?php for ($i = 1; $i <= 4; $i++):?>
        <?php echo $html->image('featured_' . $i . '.jpg');?>
    <?php endfor;?>
</div>

<script type="text/javascript">
$(function() {
    $('#featured').orbit({
        animation: 'fade',                  // fade, horizontal-slide, vertical-slide, horizontal-push
        animationSpeed: 800,                // how fast animtions are
        pauseOnHover: false, 		 // if you hover pauses the slider
        startClockOnMouseOut: false, 	 // if clock should start on MouseOut
        startClockOnMouseOutAfter: 1000, 	 // how long after MouseOut should the timer start again
        directionalNav: true, 		 // manual advancing directional navs
        captions: true, 			 // do you want captions?
        captionAnimation: 'fade', 		 // fade, slideOpen, none
        captionAnimationSpeed: 800, 	 // if so how quickly should they animate in
        bullets: false,			 // true or false to activate the bullet navigation
        bulletThumbs: false,		 // thumbnails for the bullets
    });
});
</script>
