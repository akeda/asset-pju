<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => 'Jenis jaringan',
                    'sortable' => false
                ),
                "code" => array(
                    'title' => 'Kode',
                    'sortable' => false
                )
            ),
            "editable"  => "name"
        ));
?>
</div>
