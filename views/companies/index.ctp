<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php $html->script('jquery.carouFredSel-3.2.0', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
    .carousel {
        margin-bottom: 5px;
        background-color: #fff;
    }
    .carousel ul {
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    .carousel li {
        color: #666;
        text-align: center;
        width: 30px;
        height: 48px;
        padding: 0;
        margin: 6px;
        display: block;
        float: left;
    }
    .carousel li a {
        display: block;
    }
    .carousel li a img {
        border: 2px solid #333;
    }
    .carousel li .delImage {
        font-size: 9px;
    }
    .clearfix {
        float: none;
        clear: both;
    }
    .prev {
        margin-left: 10px;
        font-weight: bold;
    }
    .next {
        float: right;
        margin-right: 10px;
        font-weight: bold;
    }
    .carousel .disabled
    {
        font-weight: normal !important;
        text-decoration: none;
        color: #999;
        cursor: default;
    }
</style>
<div class="<?php echo $this->params['controller']; ?> index">
<?php 
    echo $this->element('tablegrid', array(
            "fields" => array(
                "name" => array(
                    'title' => __('Name', true),
                    'sortable' => true
                ),
                "district_id" => array(
                    'title' => 'Kecamatan',
                    'sortable' => false
                ),
                'address' => array(
                    'title' => 'Alamat',
                    'sortable' => false
                ),
                'contact_person' => array(
                    'title' => 'Contact person',
                    'sortable' => false
                ),
                'phone_1' => array(
                    'title' => 'Telepon',
                    'sortable' => false
                ),
                'industry_category' => array(
                    'title' => 'Kategori Industri',
                    'sortable' => true
                ),
                'unit_category' => array(
                    'title' => 'Unit',
                    'sortable' => true
                ),
                'activity_type' => array(
                    'title' => 'Jenis Usaha',
                    'sortable' => false
                ),
                'actionLinks' => array(
                    'title' => '',
                    'sortable' => false
                )
            ),
            'filter' => array(
                array(
                    'district_id' => array(
                        'label' => 'Kecamatan',
                        'options' => $districts
                    ),
                    'address' => 'Jalan',
                    'contact_person' => 'Contact Person',
                    'company_type' => array(
                        'label' => 'Jenis perusahaan',
                        'options' => $company_types
                    )
                ),
                array(
                    'industry_category' => array(
                        'label' => 'Kategori industri',
                        'options' => $industry_categories
                    ),
                    'unit_category' => array(
                        'label' => 'Unit',
                        'options' => $unit_categories
                    ),
                    'name' => 'Nama perusahaan'
                )
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Pencarian data industri',
            "assoc" => array(
                'district_id' => array(
                    'model' => 'District',
                    'field' => 'name'
                )
            ),
            "editable"  => 'name'
    ));
?>
</div>
<script type="text/javascript">
var cb;
$(function() {
    cb = $('.colorbox').colorbox({
        close: "Tutup",
        onComplete: function() {
            $('.carousel ul').carouFredSel({
                circular: false,
                infinite: false,
                auto: false,
                prev: "#prev",
                next: "#next"
            });
        }
    });
});
</script>
