<?php echo $javascript->codeBlock($ajaxURL);?>
<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php $html->script('jquery.carouFredSel-3.2.0', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
    <?php if (!empty($company['CompanyImage']) ):?>
    .carousel {
        margin-bottom: 5px;
        width: 208px;
    }
    .carousel ul {
        background-color: #efefef;
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    .carousel li {
        color: #666;
        text-align: center;
        width: 30px;
        height: 48px;
        padding: 0;
        margin: 6px;
        display: block;
        float: left;
    }
    .carousel li a {
        display: block;
    }
    .carousel li a img {
        border: 2px solid #333;
    }
    .carousel li .delImage {
        font-size: 9px;
    }
    .clearfix {
        float: none;
        clear: both;
    }
    #prev {
        margin-left: 10px;
        font-weight: bold;
    }
    #next {
        float: right;
        margin-right: 10px;
        font-weight: bold;
    }
    .carousel .disabled
    {
        font-weight: normal !important;
        text-decoration: none;
        color: #999;
        cursor: default;
    }
    <?php endif;?>
</style>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Company', array(
        'enctype' => 'multipart/form-data'
      ));
?>
	<fieldset>
 		<legend>Tambah Perusahaan</legend>
        <table class="input">
            <tr>
                <!-- left -->
                <td>
                <table class="input">
                    <tr>
                        <td class="label-required">Propinsi</td>
                        <td>
                        <?php 
                            echo $form->select('province_id', $provinces, $province_id, array(
                                'div' => false, 'label'=>false, 'class'=>'required ajax_select select',
                                'rel' => '#CompanyMunicipalityId', 'ref' => 'province_id', 'empty' => ''
                                ), true);
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kotamadya / Kabupaten</td>
                        <td>
                        <?php 
                            echo $form->select('municipality_id', $municipalities, $municipality_id, array(
                                'div' => false, 'label' => false, 'class' => 'required ajax_select select',
                                'model' => 'Municipality', 'field' => 'name',
                                'rel' => '#CompanyDistrictId', 'ref' => 'municipality_id'
                                ), true);
                            echo ($form->isFieldError('municipality_id')) ? $form->error('municipality_id') : '';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kecamatan</td>
                        <td>
                        <?php 
                            echo $form->select('district_id', $districts, $district_id, array(
                                'div'=>false, 'label'=>false, 'class'=>'required ajax_select select',
                                'model' => 'District', 'field' => 'name'
                                ), true);
                            echo ($form->isFieldError('district_id')) ? $form->error('district_id') : '';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Nama perusahaan</td>
                        <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Jenis Perusahaan</td>
                        <td>
                            <?php
                            echo $form->select('company_type', $company_types, null, null, true);
                            echo ($form->isFieldError('company_type')) ? $form->error('company_type') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">NPWP</td>
                        <td><?php echo $form->input('npwp', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Alamat</td>
                        <td>
                        <?php
                            echo $form->input('address', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 3
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Contact Person</td>
                        <td>
                        <?php
                            echo $form->input('contact_person', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 2
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Telp. 1</td>
                        <td>
                        <?php
                            echo $form->input('phone_1', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Telp. 2</td>
                        <td>
                        <?php
                            echo $form->input('phone_2', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Fax</td>
                        <td>
                        <?php
                            echo $form->input('fax', array('div'=>false, 'label'=>false, 'class'=>'inputText', 'size'=>'20'));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Gambar</td>
                        <td>
                        <div class="carousel">
                        <?php
                            if (!empty($company['CompanyImage']) ):
                                echo '<ul>';
                                foreach ( $company['CompanyImage'] as $cimg):
                                    echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                                '" class="colorbox" rel="' . $company['Company']['id'] . '">' .
                                         '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                         'width="25" height="25" /></a>' .
                                         $html->link('Hapus', array('action' => 'delImage', $cimg['id'], $company['Company']['id']),
                                            array('class' => 'delImage')
                                         ) . '</li>';
                                endforeach;
                                echo '</ul>';
                                echo '<div class="clearfix"></div>';
                                echo '<a id="prev" href="#">&lt;</a>';
                                echo '<a id="next" href="#">&gt;</a>';
                            endif;
                        ?>
                        </div>
                        <div id="upload_image">
                            <?php
                                echo $form->input('imagename', array(
                                    'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                                    'name' => 'data[Company][CompanyImage][imagename][]', 'type' => 'file'
                                ));
                            ?>
                        </div>
                        <a href="#" id="add_upload_image">Tambah gambar</a>
                        </td>
                    </tr>
                </table>
                </td>
                <!-- right -->
                <td style="padding-left: 20px;">
                <table class="input">
                    <tr>
                        <td class="label-required">No. TDI/IUI</td>
                        <td>
                        <?php
                            echo $form->input('registration_no', array(
                                'div'=>false, 'label'=>false, 'class'=>'required'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Tgl. TDI/IUI</td>
                        <td>
                        <?php
                            echo $form->input('registration_date', array(
                                'div'=>false, 'label'=>false, 'class'=>'required', 'type' => 'date',
                                'dateFormat' => 'DMY', 'maxYear' => date('Y'), 'minYear' => 2000
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kategori industri</td>
                        <td>
                            <?php
                            echo $form->select('industry_category', $industry_categories, null, null, true);
                            echo ($form->isFieldError('industry_category')) ? $form->error('industry_category') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">KBLI<br />
                        <span class="label">Klasifikasi Baku <br />Lapangan Usaha Indonesia</span>
                        </td>
                        <td><?php echo $form->input('kbli', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
                    </tr>
                    <tr>
                        <td class="label-required">Kategori Unit</td>
                        <td>
                            <?php
                            echo $form->select('unit_category', $unit_categories, null, null, true);
                            echo ($form->isFieldError('unit_category')) ? $form->error('unit_category') : '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Jenis Usaha</td>
                        <td>
                        <?php
                            echo $form->input('activity_type', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 3
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Komoditi</td>
                        <td>
                        <?php
                            echo $form->input('commodity', array(
                                'div' => false, 'label' => false, 'class' => 'required', 'type' => 'textarea',
                                'rows' => 2
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">KKI<br />
                        <span class="label">Klasifikasi Komoditi Industri</span>
                        </td>
                        <td>
                        <?php
                            echo $form->input('kki', array(
                                'div' => false, 'label' => false, 'class' => 'required'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Kapasitas</td>
                        <td>
                        <?php
                            echo $form->input('commodity_capacity', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('commodity_capacity_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Satuan</span>'
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Total investasi</td>
                        <td>
                        <?php
                            echo $form->input('total_invest', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            ));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Tenaga kerja</td>
                        <td>
                        <?php
                            echo $form->input('resource_male', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; <span class="label">Laki-laki</span><br />';
                            echo '<div>&nbsp;</div>';
                            echo $form->input('resource_female', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; <span class="label">Perempuan</span>';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Limbah</td>
                        <td>
                        <?php
                            echo $form->input('solid_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('solid_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Padat</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('liquid_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('liquid_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Cair</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('gas_waste', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('gas_waste_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Gas</span>';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label-required">Pemakaian Sumber Daya</td>
                        <td>
                        <?php
                            echo $form->input('power_usage', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('power_usage_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Listrik</span><br />';
                            echo '<div>&nbsp;</div>';
                            
                            echo $form->input('water_usage', array(
                                'div' => false, 'label' => false, 'class' => 'required numeric'
                            )) . ' &nbsp; ';
                            echo $form->select('water_usage_unit', $units, null, null, true);
                            echo ' &nbsp; <span class="label">Air</span><br />';
                            echo '<div>&nbsp;</div>';
                        ?>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span><br />
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Update', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Company']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Company']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
<script type="text/javascript">
$(function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $company['Company']['name'];?></h3>" +
               "<br /><?php echo $company['Company']['address'];?>" +
               '<br /><?php echo $this->Html->link('Hapus', array('action' => 'unsetLatLng', $company['Company']['id']), array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $company['Company']['id'];?>";
    
    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);
        
        e.preventDefault();
        return false;
    });
    
    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });
    
    $('.colorbox').colorbox({
        close: "Tutup"
    });

    <?php if (!empty($company['CompanyImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
    
    function init() {
    <?php if ( !empty($company['Company']['lat']) && !empty($company['Company']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $company['Company']['lat'] . ', ' . $company['Company']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($company['Company']['lat']) && !empty($company['Company']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $company['Company']['name'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
