<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php $html->script('jquery.carouFredSel-3.2.0', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<?php $html->script('markerclusterer_compiled', array('inline' => false));?>
<style type="text/css">
#map_canvas {
    width: 100%;
    height: 600px;
    margin-right: 20px;
}
.carousel {
    margin-bottom: 5px;
    width: 208px;
}
.carousel ul {
    background-color: #efefef;
    margin: 0;
    padding: 0;
    list-style: none;
    display: block;
}
.carousel li {
    color: #666;
    text-align: center;
    width: 30px;
    height: 30px;
    padding: 0;
    margin: 6px;
    display: block;
    float: left;
}
.carousel li a {
    display: block;
}
.carousel li a img {
    border: 2px solid #333;
}
.carousel li .delImage {
    font-size: 9px;
}
.clearfix {
    float: none;
    clear: both;
}
.prev {
    margin-left: 10px;
    font-weight: bold;
}
.next {
    float: right;
    margin-right: 10px;
    font-weight: bold;
}
.carousel .disabled
{
    font-weight: normal !important;
    text-decoration: none;
    color: #999;
    cursor: default;
}
</style>
<div class="tablegrid-head">
    <div class="module-head">
        <div class="module-head-c">
            <h2>Peta sebaran industri</h2>
        </div>
    </div>
</div>
<div id="map_canvas_container" style="margin: 0 20px 10px 0;">
    <div id="map_canvas"></div>
</div>
<script type="text/javascript">
var data = <?php echo $companies;?>;
var map;
$(function() {
    function init() {
        // Kp. Melayu
        var center = new google.maps.LatLng(-6.224216,106.866757);
        
        map = new google.maps.Map(document.getElementById("map_canvas"), {
            zoom: 14,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var markers = [];
        for (var i=0, dataCompany; dataCompany = data[i]; i++) {
            if ( dataCompany['Company']['lat'] !== null && dataCompany['Company']['lng'] !== null ) {
                var latlng = new google.maps.LatLng(dataCompany['Company']['lat'], dataCompany['Company']['lng']);
                var marker = new google.maps.Marker({
                    position: latlng,
                    title: dataCompany['Company']['name']
                });
                var info = '<h3>' + dataCompany['Company']['name'] +  '</h3>' +
                           '<br />' + dataCompany['Company']['address'];
                
                info += '<div class="carousel" id="carousel_' + dataCompany['Company']['id'] + '"><ul>';
                for ( var j = 0, cimg; cimg = dataCompany['CompanyImage'][j]; j++ ) {
                    info += '<li><a rel="' + dataCompany['Company']['id'] + '" href="<?php echo $urlImage . $resizedPrefix;?>' +
                                 cimg['imagename'] + '" class="colorbox">' +
                            '<img src="<?php echo $urlImage . $thumbPrefix;?>' + cimg['imagename'] + '" />' +
                            '</a></li>';
                }
                info += '</ul><div class="clearfix"></div><a class="prev" id="prev_' + dataCompany['Company']['id'] + '" href="#">&lt;</a>';
                info += '<a class="next" id="next_' + dataCompany['Company']['id'] + '" href="#">&gt;</a>';
                info += '</div>';
                
                var infowindow = new google.maps.InfoWindow({
                    content: info,
                    size: new google.maps.Size(50,200)
                });
                var fn = markerClicked(marker, infowindow, dataCompany['Company']['id']);
                google.maps.event.addListener(marker, 'click', fn);
                markers.push(marker);
            }
        }
        var markerCluster = new MarkerClusterer(map, markers);
    }
    function markerClicked(m, iw, id) {
        return function() {
            google.maps.event.addListener(m, 'click', function() {
                iw.open(map, m);
            });
            google.maps.event.addListener(iw, 'domready', function() {
                $('#carousel_' + id + ' ul').carouFredSel({
                    circular: false,
                    infinite: false,
                    auto: false,
                    prev: "#prev_" + id,
                    next: "#next_" + id
                });
                $('.colorbox').colorbox({
                    close: "Tutup"
                });
            });
        }
    }
    init();
});
</script>
