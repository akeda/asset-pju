<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('GasType');?>
	<fieldset>
 		<legend>Tambah Jenis Bahan Bakar</legend>
        <table class="input">
            <tr>
                <td class="label-required">Jenis Bahan Bakar</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
