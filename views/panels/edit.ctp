<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Panel');?>
	<fieldset>
 		<legend>Tambah Panel</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama Jalan<br />
                <span class="label">Nama jalan
                </span>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gardu<br />
                <span class="label">Nama/kode gardu
                </span>
                <td>
                <?php 
                    echo $form->select('relay_id', $relays, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('relay_id')) ? $form->error('relay_id') : '';
                ?>
                </td>
            </tr>
            <!--
            <tr>
                <td class="label-required">Panel Induk<br />
                <span class="label">Jika ini merupakan panel pembagi<br />
                isi dengan nama panel induknya
                </span>
                </td>
                <td>
                <?php
                    echo $form->select('parent_id', $panels, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('parent_id')) ? $form->error('parent_id') : '';
                ?>
                </td>
            </tr>
            -->
            <tr>
                <td class="label-required">Tipe Panel<br />
                <span class="label">Tipe Panel
                </span>
                </td>
                <td>
                <?php
                    echo $form->select('type', $types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('type')) ? $form->error('type') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama panel<br />
                <span class="label">Nama/kode panel
                </span>
                </td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Jaringan</td>
                <td>
                <?php
                    echo '<table>';
                    foreach ($network_sizes as $kns => $ns) {
                        echo '<tr>';
                            echo '<td>' . $ns['NetworkSize']['name'] . '</td>';
                            echo '<td>' . (isset($ns['NetworkType']['name']) ?
                                    $ns['NetworkType']['name'] : '') .
                                 '</td>';
                                 
                            echo '<td>';
                            echo $form->input('network_size_id', array(
                                'div' => false, 'label' => false, 'id' => false, 'type' => 'hidden',
                                'name' => 'data[NetworkSize][' . $kns . '][network_size_id]',
                                'value' => $ns['NetworkSize']['id']
                            ));
                            echo $form->input('total', array(
                                'div' => false, 'label' => false, 'id' => false,
                                'name' => 'data[NetworkSize][' . $kns . '][total]',
                                'value' => isset($panel['PanelNetwork'][$kns]['total']) ?
                                           $panel['PanelNetwork'][$kns]['total'] : ''
                            )) . ' &nbsp; <span class="label">Total jaringan</span>';
                            echo '</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Peta<br />
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span>
                </td>
                <td>
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Update', true), array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Panel']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Panel']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
$(function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $panel['Panel']['name'];?></h3>" +
               "<br />Gardu <?php echo $panel['Relay']['name'];?>" +
               "<br /><?php echo $panel['Street']['name'];?>" +
               '<br /><?php echo $this->Html->link('Hapus', array('action' => 'unsetLatLng', $panel['Panel']['id']), array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $panel['Panel']['id'];?>";
    function init() {
    <?php if ( !empty($panel['Panel']['lat']) && !empty($panel['Panel']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $panel['Panel']['lat'] . ', ' . $panel['Panel']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($panel['Panel']['lat']) && !empty($panel['Panel']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $panel['Panel']['name'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
