<?php echo $this->element('excelfilter',
        array(
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Nama jalan',
                        'options' => $streets
                    ),
                    'relay_id' => array(
                        'label' => 'Gardu',
                        'options' => $relays
                    ),
                    'type' => array(
                        'label' => 'Tipe',
                        'options' => $types
                    )
                ),
                'name' => 'Nama panel'
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Pencarian nama panel',
        ));
?>
