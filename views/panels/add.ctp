<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Panel');?>
	<fieldset>
 		<legend>Tambah Panel</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama Jalan<br />
                <span class="label">Nama jalan
                </span>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gardu<br />
                <span class="label">Nama/kode gardu
                </span>
                <td>
                <?php 
                    echo $form->select('relay_id', $relays, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('relay_id')) ? $form->error('relay_id') : '';
                ?>
                </td>
            </tr>
            <!--
            <tr>
                <td class="label-required">Panel Induk<br />
                <span class="label">Jika ini merupakan panel pembagi<br />
                isi dengan nama panel induknya
                </span>
                </td>
                <td>
                <?php
                    echo $form->select('parent_id', $panels, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('parent_id')) ? $form->error('parent_id') : '';
                ?>
                </td>
            </tr>
            -->
            <tr>
                <td class="label-required">Tipe Panel<br />
                <span class="label">Tipe Panel
                </span>
                </td>
                <td>
                <?php
                    echo $form->select('type', $types, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('type')) ? $form->error('type') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama panel<br />
                <span class="label">Nama/kode panel
                </span>
                </td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Jaringan</td>
                <td>
                <?php
                    echo '<table>';
                    foreach ($network_sizes as $kns => $ns) {
                        echo '<tr>';
                            echo '<td>' . $ns['NetworkSize']['name'] . '</td>';
                            echo '<td>' . (isset($ns['NetworkType']['name']) ?
                                    $ns['NetworkType']['name'] : '') .
                                 '</td>';
                                 
                            echo '<td>';
                            echo $form->input('network_size_id', array(
                                'div' => false, 'label' => false, 'id' => false, 'type' => 'hidden',
                                'name' => 'data[NetworkSize][' . $kns . '][network_size_id]',
                                'value' => $ns['NetworkSize']['id']
                            ));
                            echo $form->input('total', array(
                                'div' => false, 'label' => false, 'id' => false,
                                'name' => 'data[NetworkSize][' . $kns . '][total]'
                            )) . ' &nbsp; <span class="label">Total jaringan</span>';
                            echo '</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
