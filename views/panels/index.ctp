<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
</style>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array(
            "fields" => array(
                "name" => array(
                    'title' => 'Panel',
                    'sortable' => true
                ),
                "type" => array(
                    'title' => 'Tipe',
                    'sortable' => true
                ),
                "subdistrict" => array(
                    'title' => 'Kelurahan',
                    'sortable' => false
                ),
                "street_id" => array(
                    'title' => 'Nama Jalan',
                    'sortable' => false
                ),
                "relay_id" => array(
                    'title' => 'Gardu',
                    'sortable' => false
                ),
                'total_lamps' => array(
                    'title' => 'Jumlah existing',
                    'sortable' => false
                ),
                /*
                "parent_id" => array(
                    'title' => 'Panel Induk',
                    'sortable' => false
                )*/
                'actionLinks' => array(
                    'title' => '',
                    'sortable' => false
                )
            ),
            "editable"  => "name",
            'filter' => array(
                array(
                    'subdistrict_id' => array(
                        'label' => 'Kelurahan',
                        'options' => $subdistricts
                    ),
                    'street_id' => array(
                        'label' => 'Nama jalan',
                        'options' => $streets
                    ),
                    'relay_id' => array(
                        'label' => 'Gardu',
                        'options' => $relays
                    ),
                    'type' => array(
                        'label' => 'Tipe',
                        'options' => $types
                    )
                ),
                'name' => 'Nama panel'
            ),
            'filter_max_col' => 4,
            'filter_title' => 'Pencarian nama panel',
            "assoc" => array(
                "street_id" => array(
                    'model' => 'Street',
                    'field' => 'name'
                ),
                "relay_id" => array(
                    'model' => 'Relay',
                    'field' => 'name'
                ),/*
                "parent_id" => array(
                    'model' => 'Parent',
                    'field' => 'name'
                )*/
            ),
            'replacement' => array(
                'type' => array(
                    'I' => array(
                        'replaced' => 'Induk'
                    ),
                    'P' => array(
                        'replaced' => 'Pembagi'
                    ),
                    'N' => array(
                        'replaced' => 'Natrium'
                    )
                )
            )
        ));
?>
</div>
<script type="text/javascript">
var cb;
$(function() {
    cb = $('.colorbox').colorbox({
        close: "Tutup"
    });
});
</script>
