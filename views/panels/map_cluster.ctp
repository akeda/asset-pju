<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<?php $html->script('markerclusterer_compiled', array('inline' => false));?>
<style type="text/css">
#map_canvas {
    width: 100%;
    height: 600px;
    margin-right: 20px;
}
</style>
<div class="tablegrid-head">
    <div class="module-head">
        <div class="module-head-c">
            <h2>Peta sebaran panel</h2>
        </div>
    </div>
</div>
<div id="map_canvas_container" style="margin: 0 20px 10px 0;">
    <div id="map_canvas"></div>
</div>
<script type="text/javascript">
var data = <?php echo $panels;?>;
var map;
$(function() {
    function init() {
        // Kp. Melayu
        var center = new google.maps.LatLng(-6.224216,106.866757);
        
        map = new google.maps.Map(document.getElementById("map_canvas"), {
            zoom: 14,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var markers = [];
        for (var i=0, dataPanel; dataPanel = data[i]; i++) {
            if ( dataPanel['Panel']['lat'] !== null && dataPanel['Panel']['lng'] !== null ) {
                var latlng = new google.maps.LatLng(dataPanel['Panel']['lat'], dataPanel['Panel']['lng']);
                var marker = new google.maps.Marker({
                    position: latlng,
                    title: dataPanel['Panel']['name']
                });
                var info = '<h3>' + dataPanel['Panel']['name'] + '</h3>' + 
                           '<br />Gardu ' + dataPanel['Relay']['name'] +
                           '<br />' + dataPanel['Street']['name'];
                var infowindow = new google.maps.InfoWindow({
                    content: info,
                    size: new google.maps.Size(50,200)
                });
                var fn = markerClicked(marker, infowindow);
                google.maps.event.addListener(marker, 'click', fn);
                markers.push(marker);
            }
        }
        var markerCluster = new MarkerClusterer(map, markers);
    }
    function markerClicked(m, iw) {
        return function() {
            google.maps.event.addListener(m, 'click', function() {
                iw.open(map, m);
            });
        }
    }
    init();
});
</script>
