<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Panel');?>
	<fieldset>
 		<legend>Tambah Panel</legend>
        <table class="input">
            <tr>
                <td class="label-required">Nama Jalan<br />
                <span class="label">Nama jalan
                </span>
                <td>
                <?php 
                    echo $form->select('street_id', $streets, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('street_id')) ? $form->error('street_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gardu<br />
                <span class="label">Nama/kode gardu
                </span>
                <td>
                <?php 
                    echo $form->select('relay_id', $relays, null, array(
                        'div' => false, 'label' => false, 'class' => 'required'
                        ), true);
                    echo ($form->isFieldError('relay_id')) ? $form->error('relay_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Jumlah panel induk<br />
                <span class="label">Jumlah panel induk di gardu<br />yang disebutkan di atas
                </span>
                </td>
                <td><?php echo $form->input('qty_pi', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Jumlah panel pembagi<br />
                <span class="label">Jumlah panel pembagi di gardu<br />yang disebutkan di atas
                </span>
                </td>
                <td><?php echo $form->input('qty_pp', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Add', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
