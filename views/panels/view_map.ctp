<div id="map_canvas"></div>
<script type="text/javascript">
$(document).bind('cbox_complete', function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $panel['Panel']['name'];?></h3>" +
               "<br />Gardu <?php echo $panel['Relay']['name'];?>" +
               "<br /><?php echo $panel['Street']['name'];?>";
    function init() {
    <?php if ( !empty($panel['Panel']['lat']) && !empty($panel['Panel']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $panel['Panel']['lat'] . ', ' . $panel['Panel']['lng'];?>);
    <?php else:?>
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
    <?php if ( !empty($panel['Panel']['lat']) && !empty($panel['Panel']['lng']) ): ?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $panel['Panel']['name'];?>"
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
    }
    init();
});
</script>
