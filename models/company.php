<?php
class Company extends AppModel {
	var $name = 'Company';
    var $validate = array(
        'district_id' =>  array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vDistrict'),
            'message' => 'Pilih sesuai pilihan'
        ),
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Nama kosong',
                'last' => true
            ),
            'max' => array(
                'rule' => array('maxLength', 50),
                'message' => 'Maksimal 30 karakter'
            )
        ),
        'registration_no' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'No. TDI/IUI kosong'
        ),
        'registration_date' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'Tgl. TDI/IUI tidak valid'
        ),
        'company_type' => array(
            'required' => false,
            'allowEmpty' => true,
            'rule' => array('inList', array('PT', 'CV', 'UD', '')),
            'message' => 'Pilih sesuai pilihan'
        ),
        'industry_category' => array(
            'rule' => array('inList', array(1, 2)),
            'message' => 'Pilih sesuai pilihan'
        ),
        'unit_category' => array(
            'rule' => array('inList', array(1, 2, 3)),
            'message' => 'Pilih sesuai pililhan'
        )
    );
    
 	var $belongsTo = array(
        'District',
        'CommodityCapacityUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'commodity_capacity_unit'
        ),
        'SolidWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'solid_waste_unit'
        ),
        'LiquidWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'liquid_waste_unit'
        ),
        'GasWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'gas_waste_unit'
        ),
        'PowerUsageUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'power_usage_unit'
        ),
        'WaterUsageUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'water_usage_unit'
        )
    );
    
    var $hasMany = array(
        'CompanyImage' => array(
            'dependent' => true
        ),
        'CompanyReport'
    );
    
    function afterSave($created) {
        if ( $created ) {
            $data = $this->data['Company'];
            unset($data['CompanyImage']);
            $data['company_id'] = $this->id;
            $data['report_date'] = $data['registration_date'];
            
            $this->CompanyReport->create();
            $this->CompanyReport->save($data);
        }
    }

/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vDistrict($field) {
        return $this->District->find('count', array(
            'conditions' => array(
                'District.id' => $field["district_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $contain = array(
            'District' => array(
                'fields' => array('name')
            ),
            'CompanyReport' => array(
                'order' => array('CompanyReport.report_date')
            )
        );
        
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        
        $industry_categories = array(
            1 => 'Mesin, Logam dan Kimia', 2 => 'Aneka Industri'
        );
        $unit_categories = array(
            1 => 'Kecil', 2 => 'Menengah', 3 => 'Besar'
        );
        
        foreach ( $records as $key => $record ) {
            if ( !empty($record['Company']['company_type']) ) {
                $records[$key]['Company']['name'] = $record['Company']['company_type'] . '. ' . $record['Company']['name'];
            }
            if ( !empty($record['Company']['phone_2']) ) {
                $records[$key]['Company']['phone_1'] .= ' / ' . $record['Company']['phone_2'];
            }
            if ( !empty($record['Company']['fax']) ) {
                $records[$key]['Company']['phone_1'] .= ' / Fax: ' . $record['Company']['fax'];
            }
            if ( !empty($record['Company']['industry_category']) ) {
                $records[$key]['Company']['industry_category'] = $industry_categories[ $record['Company']['industry_category'] ];
            }
            if ( !empty($record['Company']['unit_category']) ) {
                $records[$key]['Company']['unit_category'] = $unit_categories[ $record['Company']['unit_category'] ];
            }
        }
        
        return $records;
    }
}
?>
