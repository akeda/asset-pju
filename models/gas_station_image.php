<?php
class GasStationImage extends AppModel {
    var $name = 'GasStationImage';
    var $belongsTo = array(
        'GasStation'
    );
}
