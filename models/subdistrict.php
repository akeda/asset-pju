<?php
class Subdistrict extends AppModel {
    var $validate = array(
        'district_id' => array(
            'rule' => array('vDistrict'),
            'message' => 'Choose based on available option'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be empty'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 150),
                'message' => 'Maximum character is 150 characters'
            ),
        )
    );
    var $belongsTo = array(
        'District',
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vDistrict($field) {
        return $this->District->find('count', array(
            'conditions' => array(
                'District.id' => $field["district_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $fields = array('id', 'name', 'code', 'created_by', 'created');
        $contain = array(
            'User' => array(
                'fields' => array('name')
            ),
            'District' => array(
                'fields' => array('name', 'municipality_id'),
            )
        );
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        $this->District->Municipality->Behaviors->attach('Containable');
        $_municipalities = $this->District->Municipality->find('all', array(
            'fields' => array('id', 'province_id', 'name'),
            'contain' => array(
                'Province' => array('fields' => array('name'))
            )
        ));
        $municipalities = array();
        foreach ( $_municipalities as $k => $v ) {
            $municipalities[$v['Municipality']['id']]['name'] = $v['Municipality']['name'];
            $municipalities[$v['Municipality']['id']]['province'] = $v['Province']['name'];
        }
        foreach ( $records as $key => $record ) {
            $records[$key]['Municipality']['name'] = isset($municipalities[$record['District']['municipality_id']]) ?
                $municipalities[$record['District']['municipality_id']]['name'] : '';
            $records[$key]['Province']['name'] = isset($municipalities[$record['District']['municipality_id']]) ?
                $municipalities[$record['District']['municipality_id']]['province'] : '';
        }
        return $records;
    }
}
?>
