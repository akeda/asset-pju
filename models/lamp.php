<?php
class Lamp extends AppModel {
    var $validate = array(
        'subdistrict_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vSubdistrict',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'street_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vStreet',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'relay_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vRelay',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'panel_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vPanel',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'pillar_type_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vPillarType',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'lamp_type_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vLampType',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'network_size_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vNetworkSize',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'network_type_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'vNetworkType',
            'message' => 'Pilih sesuai pilihan yang ada'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Nama kosong'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 50),
                'message' => 'Maksimal karakter 50'
            ),
        )
    );
    
    var $belongsTo = array(
        'Subdistrict', 'Street', 'LampType', 'PillarType', 'PillarType', 'Relay', 'Panel', 'NetworkSize',
        'NetworkType'
    );
    
    function vStreet($field) {
        return $this->Street->find('count', array(
            'conditions' => array('id' => $field['street_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vSubdistrict($field) {
        return $this->Subdistrict->find('count', array(
            'conditions' => array('id' => $field['subdistrict_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vRelay($field) {
        return $this->Relay->find('count', array(
            'conditions' => array('id' => $field['relay_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vPanel($field) {
        return $this->Panel->find('count', array(
            'conditions' => array('id' => $field['panel_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vPillarType($field) {
        return $this->PillarType->find('count', array(
            'conditions' => array('id' => $field['pillar_type_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vLampType($field) {
        return $this->LampType->find('count', array(
            'conditions' => array('id' => $field['lamp_type_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vNetworkSize($field) {
        return $this->NetworkSize->find('count', array(
            'conditions' => array('id' => $field['network_size_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vNetworkType($field) {
        return $this->NetworkType->find('count', array(
            'conditions' => array('id' => $field['network_type_id']), 'recursive' => -1
        )) > 0;
    }
}
?>
