<?php
class CompanyReport extends AppModel {
	var $name = 'CompanyReport';
    var $validate = array(
        'district_id' =>  array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vDistrict'),
            'message' => 'Pilih sesuai pilihan'
        ),
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Nama kosong',
                'last' => true
            ),
            'max' => array(
                'rule' => array('maxLength', 50),
                'message' => 'Maksimal 30 karakter'
            )
        ),
        'registration_no' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'No. TDI/IUI kosong'
        ),
        'registration_date' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'Tgl. TDI/IUI tidak valid'
        ),
        'company_type' => array(
            'required' => false,
            'allowEmpty' => true,
            'rule' => array('inList', array('PT', 'CV', 'UD', '')),
            'message' => 'Pilih sesuai pilihan'
        ),
        'industry_category' => array(
            'rule' => array('inList', array(1, 2)),
            'message' => 'Pilih sesuai pilihan'
        ),
        'unit_category' => array(
            'rule' => array('inList', array(1, 2, 3)),
            'message' => 'Pilih sesuai pililhan'
        )
    );
    
 	var $belongsTo = array(
        'Company', 'District',
        'CommodityCapacityUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'commodity_capacity_unit'
        ),
        'SolidWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'solid_waste_unit'
        ),
        'LiquidWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'liquid_waste_unit'
        ),
        'GasWasteUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'gas_waste_unit'
        ),
        'PowerUsageUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'power_usage_unit'
        ),
        'WaterUsageUnit' => array(
            'className' => 'Unit',
            'foreignKey' => 'water_usage_unit'
        )
    );

/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vDistrict($field) {
        return $this->District->find('count', array(
            'conditions' => array(
                'District.id' => $field["district_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $contain = array(
            'District' => array(
                'fields' => array('name')
            ),
            'Company' => array(
                'fields' => array('id', 'lat', 'lng')
            )
        );
        
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        
        $industry_categories = array(
            1 => 'Mesin, Logam dan Kimia', 2 => 'Aneka Industri'
        );
        $unit_categories = array(
            1 => 'Kecil', 2 => 'Menengah', 3 => 'Besar'
        );
        
        foreach ( $records as $key => $record ) {
            if ( !empty($record['CompanyReport']['company_type']) ) {
                $records[$key]['CompanyReport']['name'] = $record['CompanyReport']['company_type'] . '. ' . $record['CompanyReport']['name'];
            }
            if ( !empty($record['CompanyReport']['phone_2']) ) {
                $records[$key]['CompanyReport']['phone_1'] .= ' / ' . $record['CompanyReport']['phone_2'];
            }
            if ( !empty($record['CompanyReport']['fax']) ) {
                $records[$key]['CompanyReport']['phone_1'] .= ' / Fax: ' . $record['CompanyReport']['fax'];
            }
            if ( !empty($record['CompanyReport']['industry_category']) ) {
                $records[$key]['CompanyReport']['industry_category'] = $industry_categories[ $record['CompanyReport']['industry_category'] ];
            }
            if ( !empty($record['CompanyReport']['unit_category']) ) {
                $records[$key]['CompanyReport']['unit_category'] = $unit_categories[ $record['CompanyReport']['unit_category'] ];
            }
        }
        
        return $records;
    }
}
?>
