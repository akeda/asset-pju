<?php
class StreetType extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Maximum characters is 20 characters'
            )
        ),
        'code' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^[a-zA-Z0-9\.]{1,1}$/',
                'message' => 'This field cannot be left blank and must be a single alphanumeric.'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 1),
                'message' => 'Maximum character is 1 character'
            ),
            'unique' => array(
                'rule' => array('unique', 'code'),
                'message' => 'Code already exist'
            ),
        )
    );
    
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
}
?>
