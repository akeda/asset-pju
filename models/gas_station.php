<?php
class GasStation extends AppModel {
	var $name = 'GasStation';
    var $validate = array(
        'district_id' =>  array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vDistrict'),
            'message' => 'Pilih sesuai pilihan'
        ),
        'subdistrict_id' =>  array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vSubdistrict'),
            'message' => 'Pilih sesuai pilihan'
        ),
        'street_id' =>  array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vStreet'),
            'message' => 'Pilih sesuai pilihan'
        ),
        'no' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Nama kosong',
                'last' => true
            ),
            'max' => array(
                'rule' => array('maxLength', 100),
                'message' => 'Maksimal 100 karakter'
            )
        )
    );
    
 	var $belongsTo = array(
        'District', 'Subdistrict', 'Street'
        
    );
    
    var $hasMany = array(
        'GasStationReport', 'GasStationImage'
    );

/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vDistrict($field) {
        return $this->District->find('count', array(
            'conditions' => array(
                'District.id' => $field["district_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function vSubdistrict($field) {
        return $this->Subdistrict->find('count', array(
            'conditions' => array('id' => $field['subdistrict_id']), 'recursive' => -1
        )) > 0;
    }
    
    function vStreet($field) {
        return $this->Street->find('count', array(
            'conditions' => array('id' => $field['street_id']), 'recursive' => -1
        )) > 0;
    }
}
?>
