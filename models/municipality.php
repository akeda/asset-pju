<?php
class Municipality extends AppModel {
    var $validate = array(
        'province_id' => array(
            'rule' => array('vProvince'),
            'message' => 'Choose based on available option'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be empty'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 150),
                'message' => 'Maximum character is 150 characters'
            ),
        )
    );
    
    var $belongsTo = array(
        'Province' => array(
            'class' => 'Province',
            'foreignKey' => 'province_id'
        ),
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vProvince($field) {
        return $this->Province->find('count', array(
            'conditions' => array(
                'Province.id' => $field["province_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
