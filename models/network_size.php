<?php
class NetworkSize extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => true,
            'rule' => 'notEmpty',
            'message' => 'Nama kosong'
        ),
        'network_type_id' => array(
            'rule' => 'vType',
            'message' => 'Jenis jaringan kosong'
        )
    );
    
    var $belongsTo = array('NetworkType');
    
    function vType($field) {
        return $this->NetworkType->find('count', array(
            'conditions' => array(
                'NetworkType.id' => $field['network_type_id']
            )
        ));
    }
}
?>
