<?php
class NetworkType extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => true,
            'rule' => 'notEmpty',
            'message' => 'Nama kosong'
        ),
        'code' => array(
            'required' => true,
            'rule' => '/^\w+$/',
            'message' => 'Kode kosong'
        )
    );
}
?>
