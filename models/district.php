<?php
class District extends AppModel {
    var $validate = array(
        'municipality_id' => array(
            'rule' => array('vMunicipality'),
            'message' => 'Choose based on available option'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be empty'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 150),
                'message' => 'Maximum character is 150 characters'
            ),
        )
    );
    var $belongsTo = array(
        'Municipality',
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vMunicipality($field) {
        return $this->Municipality->find('count', array(
            'conditions' => array(
                'Municipality.id' => $field["municipality_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $fields = array('id', 'name', 'code', 'created_by', 'created');
        $contain = array(
            'User' => array(
                'fields' => array('name')
            ),
            'Municipality' => array(
                'fields' => array('name', 'province_id'),
            )
        );
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        $provinces = $this->Municipality->Province->find('list');
        foreach ( $records as $key => $record ) {
            $records[$key]['Province']['name'] = isset($provinces[$record['Municipality']['province_id']]) ?
                $provinces[$record['Municipality']['province_id']] : '';
        }
        return $records;
    }
}
?>
