<?php
class Panel extends AppModel {
    var $validate = array(
        'street_id' => array(
            'rule' => array('vStreet'),
            'message' => 'Nama jalan kosong'
        ),
        'relay_id' => array(
            'rule' => array('vRelay'),
            'message' => 'Gardu kosong'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => array('notEmpty'),
                'message' => 'Nama kosong'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 15),
                'message' => 'Maksimal karakter 15'
            ),
        )
    );
    
    
    var $belongsTo = array(
        'Street', 'Relay',
        'Parent' => array(
            'className' => 'Panel',
            'foreignKey' => 'parent_id'
        )
    );
    
    var $hasMany = array(
        'PanelNetwork' => array(
            'dependent' => true
        )
    );
    
    function afterSave() {
        $this->PanelNetwork->deleteAll(array(
            'PanelNetwork.panel_id' => $this->id
        ));
        
        foreach ($this->data['NetworkSize'] as $key => $val) {
            $this->data['NetworkSize'][$key]['panel_id'] = $this->id;
        }
        
        $this->PanelNetwork->create();
        $this->PanelNetwork->saveAll($this->data['NetworkSize']);
    }
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vRelay($field) {
        return $this->Relay->find('count', array(
            'conditions' => array(
                'Relay.id' => $field["relay_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function vStreet($field) {
        return $this->Street->find('count', array(
            'conditions' => array(
                'Street.id' => $field["street_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>
