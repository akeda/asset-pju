<?php
class Street extends AppModel {
    var $validate = array(
        'subdistrict_id' => array(
            'rule' => array('vSubdistrict'),
            'message' => 'Choose based on available option'
        ),
        'street_type_id' => array(
            'rule' => array('vStreetType'),
            'message' => 'Choose based on available option'
        ),
        'name' => array(
            'required' => array(
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be empty'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 150),
                'message' => 'Maximum character is 150 characters'
            ),
        )
    );
    var $belongsTo = array(
        'Subdistrict', 'StreetType',
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
/**
 * Methods with v prefix are custom validation
 * rule
 */
    function vSubdistrict($field) {
        return $this->Subdistrict->find('count', array(
            'conditions' => array(
                'Subdistrict.id' => $field["subdistrict_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function vStreetType($field) {
        return $this->StreetType->find('count', array(
            'conditions' => array(
                'StreetType.id' => $field["street_type_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $fields = array('id', 'name', 'created_by', 'created');
        $contain = array(
            'User' => array(
                'fields' => array('name')
            ),
            'StreetType' => array(
                'fields' => array('name')
            ),
            'Subdistrict' => array(
                'fields' => array('name', 'district_id'),
                'District' => array(
                    'Municipality' => array(
                        'Province'
                    )
                )
            )
        );
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        $_districts = $this->Subdistrict->District->find('all', array(
            'fields' => array('id', 'name', 'municipality_id'), 'recursive' => -1
        ));
        $districts = array();
        $_municipalities = $this->Subdistrict->District->Municipality->find('all', array(
            'fields' => array('id', 'name', 'province_id'), 'recursive' => -1
        ));
        $municipalities = array();
        $_provinces = $this->Subdistrict->District->Municipality->Province->find('all', array(
            'fields' => array('id', 'name'), 'recursive' => -1
        ));
        $provinces = array();
        $max = max(count($_districts), count($_municipalities), count($_provinces));
        for ($i = 0; $i < $max; $i++) {
            if ( isset($_districts[$i]) ) {
                $districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District'];
            }
            if ( isset($_municipalities[$i]) ) {
                $municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality'];
            }
            if ( isset($_provinces[$i]) ) {
                $provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province'];
            }
        }
        
        foreach ($records as $key => $record) {
            $records[$key]['District'] = $districts[ $record['Subdistrict']['district_id'] ];
            $records[$key]['Municipality'] = $municipalities[ $records[$key]['District']['municipality_id'] ];
            $records[$key]['Province'] = $provinces[ $records[$key]['Municipality']['province_id'] ];
        }
        
        return $records;
    }
}
?>
