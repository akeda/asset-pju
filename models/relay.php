<?php
class Relay extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => true,
            'rule' => 'notEmpty',
            'message' => 'Nama kosong'
        )
    );
    
    var $hasAndBelongsToMany = array(
        'Street' => array(
            'className' => 'Street',
            'joinTable' => 'relays_streets',
            'foreignKey' => 'relay_id',
            'associationForeignKey' => 'street_id',
            'unique' => true
        ),
    );
    
    var $belongsTo = array('CoverageArea', 'CoverageService');
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {        
        // lamps grouped by relay_id
        App::import('Model', array('Lamp', 'Panel'));
        $Lamp = new Lamp;
        $Lamp->Behaviors->attach('Containable');
        $lamps = $Lamp->find('all', array(
            'fields' => array('COUNT(id) AS count, Lamp.relay_id'),
            'group' => 'Lamp.relay_id',
            'contain' => array()
        ));
        $grouped_lamps = array();
        foreach ($lamps as $lamp) {
            $grouped_lamps[$lamp['Lamp']['relay_id']] = $lamp[0]['count'];
        }
        
        // panels grouped by relay_id
        $Panel = new Panel;
        $panels = $Panel->find('all', array(
            'fields' => array('COUNT(id) AS count, Panel.relay_id'),
            'group' => 'Panel.relay_id',
            'recursive' => -1
        ));
        $grouped_panels = array();
        foreach ($panels as $panel) {
            $grouped_panels[$panel['Panel']['relay_id']] = $panel[0]['count'];
        }
        
        $binds = array(
            'hasOne' => array(
                'RelaysStreet',
                'FilterStreet' => array(
                    'className' => 'Street',
                    'foreignKey' => false,
                    'conditions' => array('FilterStreet.id = RelaysStreet.street_id')
                )
            )
        );
        $this->bindModel($binds);
        $group = array('Relay.id');
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group'
            )
        );
        
        foreach ( $records as $key => $record ) {
            $records[$key]['Relay']['total_lamps'] = '-';
            if ( isset($grouped_lamps[$record['Relay']['id']]) ) {
                $records[$key]['Relay']['total_lamps'] = $grouped_lamps[$record['Relay']['id']];
            }
            
            $records[$key]['Relay']['total_panels'] = '-';
            if ( isset($grouped_panels[$record['Relay']['id']]) ) {
                $records[$key]['Relay']['total_panels'] = $grouped_panels[$record['Relay']['id']];
            }
            
            if ( !empty($record['Street']) ) {
                $records[$key]['Relay']['street'] = '<ul>';
                foreach ( $record['Street'] as $street ) {
                    $records[$key]['Relay']['street'] .= '<li>' . $street['name'] . '</li>';
                }
                
                $records[$key]['Relay']['street'] .= '</ul>';
            }
        }
        
        return $records;
    }
    
    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $binds = array(
            'hasOne' => array(
                'RelaysStreet',
                'FilterStreet' => array(
                    'className' => 'Street',
                    'foreignKey' => false,
                    'conditions' => array('FilterStreet.id = RelaysStreet.street_id')
                )
            )
        );
        
        if ( isset($conditions["RelaysStreet.street_id"]) || isset($conditions['FilterStreet.subdistrict_id']) ) {
            $this->bindModel($binds);
        }

        $results = $this->find('count', compact(
            'conditions', 'recursive'
            )
        );
        
        return $results;
    }
}
?>
