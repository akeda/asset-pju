<?php
class NetworkSizesController extends AppController {
    var $pageTitle = 'Ukuran jaringan';
    
    function index() {
        $this->paginate['order'] = 'NetworkSize.name';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $types = $this->NetworkSize->NetworkType->find('list');
        $this->set('types', $types);
    }
}
?>
