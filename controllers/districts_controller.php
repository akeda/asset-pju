<?php
class DistrictsController extends AppController {
    var $pageTitle = 'Kecamatan';
    
    function index() {
        $this->paginate['order'] = 'District.name ASC';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__add(); 
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        parent::edit($id); 
    }
    
    function __setAdditionals() {
        $municipalities = $this->District->Municipality->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('municipalities', $municipalities);
    }
}
?>
