<?php
class ReportsController extends AppController {
    var $pageTitle = 'Report';
    var $uses = array('Lamp');

    function lamps() {
        $this->__setAdditionals();
        $filters = $this->__filtering();
        $condition = $filters['condition'];
        $str_fl = $filters['str_fl'];
        $str_fl_excel = $filters['str_fl_excel'];
        
        $this->set('str_fl', $str_fl);
        $this->set('str_fl_excel', $str_fl_excel);
        $this->set('records', $this->__getLamps($condition));
        // controller path
        $this->set('controllerURL', $this->__pathToController());
        $this->set('fusionURL', $this->webroot . 'files/swf/fusionchart/');
    }
    
    function __filtering() {
        $condition = array();
        $str_fl = '';
        $str_fl_excel = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Filter' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Filter';
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $filtered_link_excel[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition["Lamp.$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition["Lamp.$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
                $str_fl_excel = implode("&", $filtered_link_excel);
            }
        }
        return compact('condition', 'str_fl', 'str_fl_excel');
    }
    
    function viewdetail() {
        $this->Lamp->Behaviors->attach('Containable');
        
        $filters = $this->__filtering();
        $conditions = $filters['condition'];
        $str_fl = $filters['str_fl'];

        $records = $this->Lamp->find('all', array(
            'conditions' => $conditions,
            'contain' => array(
                'Subdistrict', 'Street', 'LampType', 'PillarType', 'Relay', 'Panel',
                'NetworkSize', 'NetworkType'
            )
        ));

        $this->layout = 'ajax';
        $this->set('records', $records);
    }
    
    function fusionxml() {
        $this->helpers[] = 'Xml';
        $this->layout = 'xml/default';
        Configure::write('debug', 0);
        
        $filters = $this->__filtering();
        $conditions = $filters['condition'];
        $this->set('records', $this->__getLamps($conditions));
    }
    
    function __getLamps($conditions = array()) {
        $this->Lamp->Behaviors->attach('Containable');
        $lamps = $this->Lamp->find('all', array(
            'fields' => array(
                'LampType.id', 'LampType.name', 'COUNT(lamp_type_id) AS qty'
            ),
            'group' => 'Lamp.lamp_type_id',
            'conditions' => $conditions,
            'contain' => array(
                'LampType.name'
            )
        ));
        $rows = array(); $i = 0;
        foreach ($lamps as $lamp) {
            $rows[$i]['id'] = $lamp['LampType']['id'];
            $rows[$i]['name'] = $lamp['LampType']['name'];
            $rows[$i]['qty'] = $lamp[0]['qty'];
            $i++;
        }
        return $rows;
    }
    
    function __setAdditionals() {
        $subdistricts = $this->Lamp->Subdistrict->find('list', array('order' => 'name ASC'));
        $streets = $this->Lamp->Street->find('list', array('order' => 'name ASC'));
        $relays = $this->Lamp->Relay->find('list', array('order' => 'name ASC'));
        $panels = $this->Lamp->Panel->find('list', array('order' => 'name ASC'));
        $lamp_types = $this->Lamp->LampType->find('list', array('order' => 'name ASC'));
        $network_types = $this->Lamp->NetworkType->find('list', array('order' => 'name ASC'));
        $network_sizes = $this->Lamp->NetworkSize->find('list', array('order' => 'name ASC'));
        
        $this->set(compact(
            'subdistricts', 'streets', 'relays', 'lamp_types', 'panels', 'network_types',
            'network_sizes'
        ));
    }
}
?>
