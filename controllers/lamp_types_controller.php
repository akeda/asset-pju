<?php
class LampTypesController extends AppController {
    var $pageTitle = 'Jenis Lampu';
    
    function index() {
        $this->paginate['order'] = 'LampType.name';
        parent::index();
    }
}
?>
