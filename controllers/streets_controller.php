<?php
class StreetsController extends AppController {
    var $pageTitle = 'Nama Jalan';
    
    function index() {
        $this->__setAdditionals();
        $this->paginate['order'] = 'Street.name ASC';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__add(); 
    }
    
    function edit($id = null) {
        $this->__setAdditionals($id);
        parent::edit($id); 
    }
    
    function __setAdditionals($id = null) {
        $province_id = null;
        $municipality_id = null;
        $district_id = null;
        $subdistrict_id = null;
        
        $provinces = $__provinces = array();
        $municipalities = $__municipalities = array();
        $districts = $__districts = array();
        $subdistricts = $__subdistricts = array();
        
        $_provinces = $this->Street->Subdistrict->District->Municipality->Province->find('all', array(
            'order' => 'Province.name'
        ));
        $_subdistricts = $this->Street->Subdistrict->find('all', array(
            'fields' => array('id', 'name', 'district_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_districts = $this->Street->Subdistrict->District->find('all', array(
            'fields' => array('id', 'name', 'municipality_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_municipalities = $this->Street->Subdistrict->District->Municipality->find('all', array(
            'fields' => array('id', 'name', 'province_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_provinces = $this->Street->Subdistrict->District->Municipality->Province->find('all', array(
            'fields' => array('id', 'name'), 'recursive' => -1,
            'order' => 'name'
        ));
        $street_types = $this->Street->StreetType->find('list', array(
            'order' => 'StreetType.name'
        ));
        $max = max(count($_subdistricts), count($_districts), count($_municipalities), count($_provinces));
        for ($i = 0; $i < $max; $i++) {
            if ( isset($_subdistricts[$i]) ) {
                $subdistricts[ $_subdistricts[$i]['Subdistrict']['id'] ] = $_subdistricts[$i]['Subdistrict']['name'];
                $__subdistricts[ $_subdistricts[$i]['Subdistrict']['id']  ] = $_subdistricts[$i]['Subdistrict'];
            }
            if ( isset($_districts[$i]) ) {
                $districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District']['name'];
                $__districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District'];
            }
            if ( isset($_municipalities[$i]) ) {
                $municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality']['name'];
                $__municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality'];
            }
            if ( isset($_provinces[$i]) ) {
                $provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province']['name'];
                $__provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province'];
            }
        }
        if ( $id ) {
            $street = $this->Street->find('first', array(
                'conditions' => array(
                    'Street.id' => $id
                ),
                'recursive' => -1
            ));
            
            if ( !empty($id) ) {
                $subdistrict = $__subdistricts[ $street['Street']['subdistrict_id'] ];
                $subdistrict_id =  $subdistrict['id'];
                $district = $__districts[ $subdistrict['district_id'] ];
                $district_id = $district['id'];
                $municipality = $__municipalities[ $district['municipality_id'] ];
                $municipality_id = $municipality['id'];
                $province = $__provinces[ $municipality['province_id'] ];
                $province_id = $province['id'];
            }
        }
                 
        $this->set('street_types', $street_types);
        
        $this->set('provinces', $provinces);
        $this->set('municipalities', $municipalities);
        $this->set('districts', $districts);
        $this->set('subdistricts', $subdistricts);
        
        $this->set('province_id', $province_id);
        $this->set('municipality_id', $municipality_id);
        $this->set('district_id', $district_id);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "';");
    }
}
?>
