<?php
class PanelsController extends AppController {
    var $pageTitle = 'Panel';
    
    function index() {
        $this->__setAdditionals();
        
        $condition = array();
        $records = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';
            
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                    
                    if ( $param_name == 'subdistrict_id' ) {
                        unset($condition[$this->modelName.".subdistrict_id"]);
                        $equal = Sanitize::paranoid($value);
                        $condition["Street.subdistrict_id"] = $equal;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
        }
        $this->paginate['order'] = 'Panel.name ASC';
        $records = $this->paginate($condition);
        $grouped_lamps = array();
        if ( !empty($records) ) {
            App::import('Model', 'Lamp');
            $Lamp = new Lamp;
            $lamps = $Lamp->find('all', array(
                'group' => 'Lamp.panel_id',
                'fields' => array('COUNT(id) AS count, Lamp.panel_id'),
                'recursive' => -1
            ));
            foreach ($lamps as $lamp) {
                $grouped_lamps[$lamp['Lamp']['panel_id']] = $lamp[0]['count'];
            }
        }
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            $records[$key]['Panel']['subdistrict'] = $this->subdistricts[$record['Street']['subdistrict_id']];
            $records[$key]['Panel']['total_lamps'] = '-';
            if ( isset($grouped_lamps[$record['Panel']['id']]) ) {
                $records[$key]['Panel']['total_lamps'] = $grouped_lamps[$record['Panel']['id']];
            }
            
            // link to map
            $records[$key]['Panel']['actionLinks'] = '';
            if ( !empty($record['Panel']['lat']) && !empty($record['Panel']['lng']) ) {
                $records[$key]['Panel']['actionLinks'] = $html->link('Lihat peta', array('action' => 'viewMap', 
                                                            $record['Panel']['id']
                                                         ), array('class' => 'colorbox')
                                                         );
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function excel() {
        $this->helpers[] = 'excel';
        $this->set('titleExcel', 'Panel');
        $this->set('templateExcel', 'panels');
        
        $this->__setAdditionals();
        $conditions = array();

        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Print' ) {
            $this->layout = 'ajax';
            Configure::write('debug', 0);
            
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Print';
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $conditions[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $conditions[$this->modelName.".$param_name LIKE"] = $like;
                    }
                    
                    if ( $param_name == 'subdistrict_id' ) {
                        unset($conditions[$this->modelName.".subdistrict_id"]);
                        $equal = Sanitize::paranoid($value);
                        $conditions["Street.subdistrict_id"] = $equal;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
            $this->set('str_fl', $str_fl);
            $records = $this->Panel->find('all', array(
                'conditions' => $conditions,
                'order' => 'Panel.name ASC'
            ));
            
            foreach ($records as $key => $record) {
                $records[$key]['Panel']['subdistrict'] = $this->subdistricts[$record['Street']['subdistrict_id']];
            }
            $this->set('data', $records);
        }
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        $panel = $this->Panel->find('first', array(
            'conditions' => array(
                'Panel.id' => $id
            )
        ));
        $network_sizes = array();
        foreach ($panel['PanelNetwork'] as $key => $val) {
            $network_sizes[$val['network_size_id']] = $val;
        }
        $panel['PanelNetwork'] = $network_sizes;
        
        if ( empty($panel) ) {
            $this->Session->setFlash('Panel tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set('panel', $panel);
        parent::edit($id); 
    }
    
    function add_bulk() {
        $this->__setAdditionals();
        
        if ( !empty($this->data) ) {
            $messageFlashSuccess = 'Data panel berhasil ditambahkan semua';
            $messageFlashError   = 'Data panel tidak berhasil ditambahkan semua';
            
            // qty panel induk
            $qty = $this->data[$this->modelName]['qty_pi'];
            if (!is_numeric($qty) || $qty <= 0) {
                $messageFlashError = 'Jumlah panel induk harus benar';
                $this->Session->setFlash($messageFlashError, 'error');
                $this->redirect(array('action' => 'add_bulk'));
            }
            unset($this->data[$this->modelName]['qty_pi']);
            // qty panel pembagi
            $qty_pp = $this->data[$this->modelName]['qty_pp'];
            if (!is_numeric($qty_pp) || $qty_pp <= 0) {
                $messageFlashError = 'Jumlah panel pembagi harus benar';
                $this->Session->setFlash($messageFlashError, 'error');
                $this->redirect(array('action' => 'add_bulk'));
            }
            unset($this->data[$this->modelName]['qty_pp']);
            
            // get count of panel induk
            $count = $this->Panel->find('count', array(
                'conditions' => array(
                    'Panel.relay_id' => $this->data[$this->modelName]['relay_id'],
                    'Panel.parent_id' => NULL
                )
            ));
            // get count of panel pembagi
            $count_pp = $this->Panel->find('count', array(
                'conditions' => array(
                    'Panel.relay_id' => $this->data[$this->modelName]['relay_id'],
                    'Panel.parent_id >' => 0
                )
            ));
            
            $data = $this->data;
            $saved_data = array();
            $failed = false;
            // create panel induk first
            for ($i = 1; $i <= $qty; $i++) {
                $this->{$this->modelName}->create();
                $data[$this->modelName]['name'] = $this->relays[$data[$this->modelName]['relay_id']] . '/PI' .
                                                  str_pad($count+$i, 2, '0', STR_PAD_LEFT);
                if (!$this->{$this->modelName}->save($data)) {
                    $failed = true;
                    break;
                } else {
                    $saved_data[] = $this->{$this->modelName}->id;
                }
            }
            // create panel pembagi
            $data = $this->data;
            // create panel induk first
            for ($i = 1; $i <= $qty_pp; $i++) {
                $this->{$this->modelName}->create();
                $data[$this->modelName]['name'] = $this->relays[$data[$this->modelName]['relay_id']] . '/PP' .
                                                  str_pad($count+$i, 2, '0', STR_PAD_LEFT);
                $data[$this->modelName]['parent_id'] = $saved_data[0];
                if (!$failed && !$this->{$this->modelName}->save($data)) {
                    $failed = true;
                    break;
                } else {
                    $saved_data[] = $this->{$this->modelName}->id;
                }
            }
            
            if (!$failed) {
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->__redirect();
            } else {
                $this->{$this->modelName}->delete($saved_data);
                $this->Session->setFlash($messageFlashError, 'error');
            }
        }
    }
    
    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $panel = $this->Panel->find('first', array(
            'conditions' => array(
                'Panel.id' => $id
            )
        ));
        if ( empty($panel) ) {
            $this->Session->setFlash('Panel tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set(compact('panel'));
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->Panel->id = $id;
        if ( $this->Panel->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->Panel->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->Panel->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $panels = json_encode($this->Panel->find('all'));
        $this->set(compact('panels'));
    }
    
    function mapClusterJson() {
        $this->layout = 'json/default';
        Configure::write('debug', 2);
        
        $panels = json_encode($this->Panel->find('all'));
        $this->set(compact('panels'));
    }
    
    function __setAdditionals() {
        $subdistricts = $this->Panel->Street->Subdistrict->find('list', array('order' => 'name ASC'));
        $this->subdistricts = $subdistricts;
        $streets = $this->Panel->Street->find('list', array('order' => 'name ASC'));
        $panels = $this->Panel->find('list', array('order' => 'name ASC'));
        $relays = $this->Panel->Relay->find('list', array(
            'order' => 'name ASC', 'recursive' => -1
        ));
        $types = array('I' => 'Induk', 'P' => 'Pembagi', 'N' => 'Natrium');
        $_network_sizes = $this->Panel->PanelNetwork->NetworkSize->find('all', array(
            'order' => 'NetworkSize.name ASC'
        ));
        $network_sizes = array();
        foreach ($_network_sizes as $key => $val) {
            $network_sizes[$val['NetworkSize']['id']] = $val;
        }
        $network_types = $this->Panel->PanelNetwork->NetworkSize->NetworkType->find('list', array(
            'order' => 'name ASC', 'recursive' => -1
        ));
        
        $this->set(compact(
            'subdistricts', 'streets', 'panels', 'relays', 'types', 'network_sizes', 'network_types'
        ));
        $this->set('urlController', $this->__pathToController());
    }
}
?>
