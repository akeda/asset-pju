<?php
class GasStationsController extends AppController {
    var $pageTitle = 'SPBU';
    
    // property related to upload image
    var $pathPrefix;
    var $pathImage;
    var $fieldImage;
    var $errorUpload;
    var $resizedPrefix;
    var $thumbPrefix;
    
    function beforeFilter() {
        $this->pathPrefix  = WWW_ROOT . 'files' . DS . 'gas_station_photos';
        $this->pathImage = $this->pathPrefix . DS;
        $this->urlImage = $this->webroot . 'files/gas_station_photos/';
        $this->fieldImage = 'imagename';
        $this->resizedPrefix = 'thumb_';
        $this->thumbPrefix = 'thumbsmall_';
        parent::beforeFilter();
    }
    
    function index() {
        $this->__setAdditionals();
        
        $condition = array();
        $records = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';

            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
        }
        $this->paginate['order'] = 'GasStation.no ASC';
        $records = $this->paginate($condition);
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            // link to map
            $records[$key]['GasStation']['actionLinks'] = '';
            if ( !empty($record['GasStation']['lat']) && !empty($record['GasStation']['lng']) ) {
                $records[$key]['GasStation']['actionLinks'] = $html->link('Lihat peta', array('action' => 'viewMap', 
                                                            $record['GasStation']['id']
                                                         ), array('class' => 'colorbox')
                                                         );
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function excel() {
        $this->helpers[] = 'excel';
        $this->set('titleExcel', 'Exisiting');
        $this->set('templateExcel', 'lamps');
        
        $this->__setAdditionals();
        $condition = array();

        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Print' ) {
            $this->layout = 'ajax';
            Configure::write('debug', 0);
            
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Print';
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
            $this->set('str_fl', $str_fl);
            $data = $this->{$this->modelName}->find('all', array(
                'conditions' => $condition
            ));
            $this->set('data', $data);
        }
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__saving();
    }
    
    function edit($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        }
        
		$this->set('id', $id);
        $this->__setAdditionals($id);
        $this->__saving($id);
        
        if (empty($this->data)) {
			$this->data = $this->{$this->modelName}->find('first', array(
                'conditions' => array($this->modelName . '.id' => $id)
            ));
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->__redirect('index');
            }
		} else {
            $this->data[$this->modelName]['id'] = $id;
        }
    }
    
    function delImage($id, $gas_station_id) {
        $this->Session->setFlash('Gambar tidak berhasil terhapus', 'error');
        
        $this->GasStation->GasStationImage->id = $id;
        $imagename = $this->GasStation->GasStationImage->field($this->fieldImage);
        if ( $this->GasStation->GasStationImage->delete($id) ) {
            @unlink($this->pathPrefix . DS . $imagename);
            @unlink($this->pathPrefix . DS . $this->resizedPrefix . $imagename);
            @unlink($this->pathPrefix . DS . $this->thumbPrefix . $imagename);
            $this->Session->setFlash('Gambar berhasil terhapus', 'success');
        }
        $this->redirect(array('action' => 'edit', $gas_station_id));
    }
    
    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $gas_station = $this->GasStation->find('first', array(
            'conditions' => array(
                'GasStation.id' => $id
            )
        ));
        if ( empty($gas_station) ) {
            $this->Session->setFlash('SPBU tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set(compact('gas_station'));
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->GasStation->id = $id;
        if ( $this->GasStation->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->GasStation->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->GasStation->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $gas_station = json_encode($this->GasStation->find('all'));
        $this->set(compact('gas_station'));
    }
    
    function __handleUploadImage($error_code, $tmp_name, $image_name) {
        $error = array();
        $possible_errors = array(
            1 => 'php.ini max file exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached'
        );
        
        // check for PHP's built-in uploading errors
        if ( $error_code > 0 ) {
            $error[] = $possible_errors[$error_code];
        }
        
        // check that the file we're working on really was the subject of an HTTP upload
        if ( !@is_uploaded_file($tmp_name) ) {
            $error[] = 'Not an HTTP upload';
        }
        
        // check if uploaded file is in fact an image
        if ( !@getimagesize($tmp_name) ) {
            $error[] = 'Only image uploads are allowed';
        }
        
        // check if our target directory is writeable
        if ( !is_writable($this->pathImage) ) {
            $error[] = 'Target upload is not writeable.';
        }
        
        if ( empty($error) ) {
            // Make a unique filename for the uploaded files and check it is not already
            // taken.
            
            // first check if we could use the original image name
            $image_path = $this->pathImage . $image_name;
            if ( file_exists($image_path) ) {
                $image_name = time() . '_' . $image_name;
                
                // uses time and iterate until it found unique name
                $image_path = $this->pathImage . $image_name;
                if ( file_exists($image_path) ) {
                    $counter = 1;
                    while ( file_exists($image_path = $this->pathImage . $counter . '_' . $image_name ) ) {
                        $counter++;
                    }
                    $image_name = $counter . '_' . $image_name;
                }
            }
            
            // error moving uploaded file
            if ( !@move_uploaded_file($tmp_name, $image_path) ) {
                $this->errorUpload = 'Receiving directory insufficient permission';
                
                return false;
            }
            
            // resizing
            $this->__resizeImage($image_name);
            
            return $image_name;
        } else {
            $this->errorUpload = $error;
            return false;
        }
    }
    
    function __saving($id = null) {
        if ( !empty($this->data) ) {
			$messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __('successfully added', true);
			$messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                __('cannot add this new record. Please fix the errors mentioned belows', true);
            if ( $id ) {
                $messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("successcully edited", true);
                $messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                    __("cannot save this modification. Please fix the errors mentioned belows", true);
            }
            
            if ( $id ) {
                $this->GasStation->id = $id;
            } else {
                $this->GasStation->create();
            }
            $gas_stations = $this->data;
            
			if ( $this->GasStation->save($this->data) ) {
                if ( isset($this->data['GasStation']['GasStationImage']['imagename']['name']) && 
                     !empty($this->data['GasStation']['GasStationImage']['imagename']['name']) ) {
                    $imgname = $this->data['GasStation']['GasStationImage']['imagename']['name'];
                    $tmps = $this->data['GasStation']['GasStationImage']['imagename']['tmp_name'];
                    $errors = $this->data['GasStation']['GasStationImage']['imagename']['error'];
                    
                    foreach ($imgname as $key => $name) {
                        if ( ($s = $this->__handleUploadImage($errors[$key], $tmps[$key], $name)) !== FALSE ) {
                            $saved_img = array(
                                'imagename' => $s, 'gas_station_id' => $this->GasStation->id
                            );
                            $this->GasStation->GasStationImage->create();
                            $this->GasStation->GasStationImage->save($saved_img);
                        }
                    }
                }
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->redirect(array('action' => 'edit', $this->GasStation->id));
			} else {
                $this->set('gas_stations', $gas_stations['GasStation']);
                $this->Session->setFlash($messageFlashError, 'error');
			}
		}
    }
    
    function __setAdditionals($id = null) {
        $districts = $this->GasStation->District->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('districts', $districts);
        $this->districts = $districts;
        
        $subdistricts = $this->GasStation->Subdistrict->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('subdistricts', $subdistricts);
        $this->subdistricts = $subdistricts;
        
        $streets = $this->GasStation->Street->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('streets', $streets);
        $this->streets = $streets;
        
        $types = $this->GasStation->GasStationReport->GasType->find('list', array('order' => 'name ASC'));
        $this->set('types', $types);
        $this->types = $types;
        
        if ( $id ) {
            $this->GasStation->Behaviors->attach('Containable');
            $gas_station = $this->GasStation->find('first', array(
                'conditions' => array(
                    'GasStation.id' => $id
                ),
                'contain' => array(
                    'GasStationImage'
                )
            ));
            $this->set('gas_station', $gas_station);
        }
        
        // images related
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "';");
        $this->set('urlController', $this->__pathToController());
    }
}
?>
