<?php
class RelaysController extends AppController {
    var $pageTitle = 'Gardu';
    
    // property related to upload image
    var $pathPrefix;
    var $pathImage;
    var $fieldImage;
    var $errorUpload;
    var $resizedPrefix;
    var $thumbPrefix;
    
    function beforeFilter() {
        $this->pathPrefix  = WWW_ROOT . 'files' . DS . 'relay_photos';
        $this->pathImage = $this->pathPrefix . DS;
        $this->urlImage = $this->webroot . 'files/relay_photos/';
        $this->fieldImage = 'imagename';
        $this->resizedPrefix = 'thumb_';
        $this->thumbPrefix = 'thumbsmall_';
        parent::beforeFilter();
    }
    
    function index() {
        $this->__setAdditionals();
        $records = array();
        $condition = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';
            
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                    
                    if ( $param_name == 'street_id' ) {
                        unset($condition[$this->modelName.".street_id"]);
                        $equal = Sanitize::paranoid($value);
                        $condition["RelaysStreet.street_id"] = $equal;
                    }
                    
                    if ( $param_name == 'subdistrict_id' ) {
                        unset($condition[$this->modelName.".subdistrict_id"]);
                        $equal = Sanitize::paranoid($value);
                        $condition["FilterStreet.subdistrict_id"] = $equal;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }   
        }
        $this->paginate['order'] = 'Relay.name ASC';
        $records = $this->paginate($condition);
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            $records[$key]['Relay']['actionLinks'] = '';
            if ( !empty($record['Relay']['lat']) && !empty($record['Relay']['lng']) ) {
                $records[$key]['Relay']['actionLinks'] = $html->link('Lihat peta', array('action' => 'viewMap', 
                                                            $record['Relay']['id']
                                                         ), array('class' => 'colorbox')
                                                         ) . ' &nbsp; ';
            }
            
            if ( !empty($record['Relay']['imagename']) ) {
                $records[$key]['Relay']['actionLinks'] .= $html->link('Lihat gambar', '/files/relay_photos/' .
                                                            $record['Relay']['imagename'],
                                                            array('class' => 'colorbox')
                                                          );
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function add() {
        $this->__setAdditionals();
        
        // upload
        if ( !empty($this->data['Relay'][$this->fieldImage]) && $this->__handleUploadImage() ) {
            
        }
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        
        $relay = $this->Relay->find('first', array(
            'conditions' => array(
                'Relay.id' => $id
            )
        ));
        if ( empty($relay) ) {
            $this->Session->setFlash('Gardu tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $streets = '<ul>';
        if ( isset($relay['Street']) && !empty($relay['Street']) ) {
            foreach ($relay['Street'] as $s) {
               $streets .= "<li>{$s['name']}</li>";
            }
        }
        $streets .= '</ul>';
        $this->set('relay', $relay);
        $this->set('streets_iw', $streets);
        
        // upload
        if ( !empty($this->data['Relay'][$this->fieldImage]) && $this->__handleUploadImage() ) {
            
        }
        parent::edit($id);
    }
    
    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $relay = $this->Relay->find('first', array(
            'conditions' => array(
                'Relay.id' => $id
            )
        ));
        if ( empty($relay) ) {
            $this->Session->setFlash('Gardu tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $streets = '<ul>';
        if ( isset($relay['Street']) && !empty($relay['Street']) ) {
            foreach ($relay['Street'] as $s) {
               $streets .= "<li>{$s['name']}</li>";
            }
        }
        $streets .= '</ul>';
        
        $this->set('relay', $relay);
        $this->set('streets_iw', $streets);
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->Relay->id = $id;
        if ( $this->Relay->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->Relay->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->Relay->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $this->Relay->Behaviors->attach('Containable');
        $relays = $this->Relay->find('all', array(
            'contain' => array('Street')
        ));
        
        foreach ($relays as $key => $relay) {
            $relays[$key]['Relay']['street'] = '<ul>';
            foreach ($relay['Street'] as $s) {
               $relays[$key]['Relay']['street'] .= "<li>{$s['name']}</li>";
            }
            $relays[$key]['Relay']['street'] .= '</ul>';
            unset($relays[$key]['Street']);
        }
        $relays = json_encode($relays);
        $this->set(compact('relays'));
    }
    
    function mapClusterJson() {
        $this->layout = 'json/default';
        Configure::write('debug', 2);
        
        $this->Relay->Behaviors->attach('Containable');
        $relays = $this->Relay->find('all', array(
            'contain' => array('Street')
        ));
        
        foreach ($relays as $key => $relay) {
            $relays[$key]['Relay']['street'] = '<ul>';
            foreach ($relay['Street'] as $s) {
               $relays[$key]['Relay']['street'] .= "<li>{$s['name']}</li>";
            }
            $relays[$key]['Relay']['street'] .= '</ul>';
            unset($relays[$key]['Street']);
        }
        $relays = json_encode($relays);
        $this->set(compact('relays'));
    }
    
    function excel() {
        $this->helpers[] = 'excel';
        $this->set('titleExcel', 'Gardu');
        $this->set('templateExcel', 'relays');
        
        $this->__setAdditionals();
        $conditions = array();

        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Print' ) {
            $this->layout = 'ajax';
            Configure::write('debug', 0);
            
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Print';
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $conditions[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $conditions[$this->modelName.".$param_name LIKE"] = $like;
                    }
                    
                    if ( $param_name == 'street_id' ) {
                        unset($conditions[$this->modelName.".street_id"]);
                        $equal = Sanitize::paranoid($value);
                        $conditions["RelaysStreet.street_id"] = $equal;
                    }
                    
                    if ( $param_name == 'subdistrict_id' ) {
                        unset($conditions[$this->modelName.".subdistrict_id"]);
                        $equal = Sanitize::paranoid($value);
                        $conditions["FilterStreet.subdistrict_id"] = $equal;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
            $this->set('str_fl', $str_fl);
            $binds = array(
                'hasOne' => array(
                    'RelaysStreet',
                    'FilterStreet' => array(
                        'className' => 'Street',
                        'foreignKey' => false,
                        'conditions' => array('FilterStreet.id = RelaysStreet.street_id')
                    )
                )
            );
            $this->Relay->bindModel($binds);
            $group = array('Relay.id');
            $records = $this->Relay->find('all', compact(
                'conditions', 'group'
                )
            );
            
            foreach ( $records as $key => $record ) {
                if ( !empty($record['Street']) ) {
                    $_street = array();
                    foreach ( $record['Street'] as $street ) {
                        $_street[] = $street['name'];
                    }
                    $records[$key]['Relay']['street'] = implode(', ', $_street);
                }
            }
            $this->set('data', $records);
        }
    }
    
    function __setAdditionals() {
        $subdistricts = $this->Relay->Street->Subdistrict->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('subdistricts', $subdistricts);
        $this->subdistricts = $subdistricts;
        
        $streets = $this->Relay->Street->find('list', array('order' => 'name ASC'));
        $this->set('streets', $streets);
        
        $coverage_areas = $this->Relay->CoverageArea->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('coverage_areas', $coverage_areas);
        
        $coverage_services = $this->Relay->CoverageService->find('list', array('order' => 'name ASC'));
        $this->set('coverage_services', $coverage_services);
        
        $this->set('pathImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        
        $this->set('urlController', $this->__pathToController());
    }
    
    function __handleUploadImage() {
        $error = array();
        $possible_errors = array(
            1 => 'php.ini max file exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached'
        );
        
        // check for PHP's built-in uploading errors
        $error_code = $_FILES['data']['error']['Relay'][$this->fieldImage];
        if ( $error_code > 0 ) {
            $error[] = $possible_errors[$error_code];
        }
        
        // check that the file we're working on really was the subject of an HTTP upload
        $tmp_name = $_FILES['data']['tmp_name']['Relay'][$this->fieldImage];
        if ( !@is_uploaded_file($tmp_name) ) {
            $error[] = 'Not an HTTP upload';
        }
        
        // check if uploaded file is in fact an image
        if ( !@getimagesize($tmp_name) ) {
            $error[] = 'Only image uploads are allowed';
        }
        
        // check if our target directory is writeable
        if ( !is_writable($this->pathImage) ) {
            $error[] = 'Target upload is not writeable.';
        }
        
        if ( empty($error) ) {
            // Make a unique filename for the uploaded files and check it is not already
            // taken.
            $image_name = $_FILES['data']['name']['Relay'][$this->fieldImage];
            
            // first check if we could use the original image name
            $image_path = $this->pathImage . $image_name;
            if ( file_exists($image_path) ) {
                $image_name = time() . '_' . $image_name;
                
                // uses time and iterate until it found unique name
                $image_path = $this->pathImage . $image_name;
                if ( file_exists($image_path) ) {
                    $counter = 1;
                    while ( file_exists($image_path = $this->pathImage . $counter . '_' . $image_name ) ) {
                        $counter++;
                    }
                    $image_name = $counter . '_' . $image_name;
                }
            }
            
            // error moving uploaded file
            if ( !@move_uploaded_file($tmp_name, $image_path) ) {
                $this->data['Relay'][$this->fieldImage] = '';
                $this->errorUpload = 'Receiving directory insufficient permission';
                
                return false;
            }
            
            // resizing
            $this->__resizeImage($image_name);
            
            // change field of image to point to our filename
            $this->data['Relay'][$this->fieldImage] = $image_name;
            
            return true;
        } else {
            $this->data['Relay'][$this->fieldImage] = '';
            $this->errorUpload = $error;
            
            return false;
        }
    }
}
?>
