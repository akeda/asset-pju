<?php
class SubdistrictsController extends AppController {
    var $pageTitle = 'Kelurahan';
    
    function index() {
        $this->paginate['order'] = 'Subdistrict.name ASC';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__add(); 
    }
    
    function edit($id = null) {
        $this->__setAdditionals($id);
        parent::edit($id); 
    }
    
    function __setAdditionals($id = null) {
        $province_id = null;
        $municipality_id = null;
        $district_id = null;
        
        $provinces = $__provinces = array();
        $municipalities = $__municipalities = array();
        $districts = $__districts = array();
        
        $_provinces = $this->Subdistrict->District->Municipality->Province->find('all', array(
            'order' => 'Province.name ASC'
        ));
        $_districts = $this->Subdistrict->District->find('all', array(
            'fields' => array('id', 'name', 'municipality_id'), 'recursive' => -1,
            'order' => 'name ASC'
        ));
        $_municipalities = $this->Subdistrict->District->Municipality->find('all', array(
            'fields' => array('id', 'name', 'province_id'), 'recursive' => -1,
            'order' => 'name ASC'
        ));
        $_provinces = $this->Subdistrict->District->Municipality->Province->find('all', array(
            'fields' => array('id', 'name'), 'recursive' => -1,
            'order' => 'name ASC'
        ));
        
        $max = max(count($_districts), count($_municipalities), count($_provinces));
        for ($i = 0; $i < $max; $i++) {
            if ( isset($_districts[$i]) ) {
                $districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District']['name'];
                $__districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District'];
            }
            if ( isset($_municipalities[$i]) ) {
                $municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality']['name'];
                $__municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality'];
            }
            if ( isset($_provinces[$i]) ) {
                $provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province']['name'];
                $__provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province'];
            }
        }
        if ( $id ) {
            $subdistrict = $this->Subdistrict->find('first', array(
                'conditions' => array(
                    'Subdistrict.id' => $id
                ),
                'recursive' => -1
            ));
            
            if ( !empty($id) ) {
                $district = $__districts[ $subdistrict['Subdistrict']['district_id'] ];
                $district_id = $district['id'];
                $municipality = $__municipalities[ $district['municipality_id'] ];
                $municipality_id = $municipality['id'];
                $province = $__provinces[ $municipality['province_id'] ];
                $province_id = $province['id'];
            }
        }
        
        $this->set('provinces', $provinces);
        $this->set('municipalities', $municipalities);
        $this->set('districts', $districts);
        
        $this->set('province_id', $province_id);
        $this->set('municipality_id', $municipality_id);
        $this->set('district_id', $district_id);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "';");
    }
}
?>
