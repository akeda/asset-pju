<?php
class PillarTypesController extends AppController {
    var $pageTitle = 'Jenis Tiang';
    
    function index() {
        $this->paginate['order'] = 'PillarType.name';
        parent::index();
    }
}
?>
