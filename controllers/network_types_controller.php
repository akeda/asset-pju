<?php
class NetworkTypesController extends AppController {
    var $pageTitle = 'Jenis Jaringan';
    
    function index() {
        $this->paginate['order'] = 'NetworkType.name';
        parent::index();
    }
}
?>
