<?php
class CompanyReportsController extends AppController {
    var $pageTitle = 'Pelaporan Industri';
    
    // property related to upload image
    var $pathPrefix;
    var $pathImage;
    var $fieldImage;
    var $errorUpload;
    var $resizedPrefix;
    var $thumbPrefix;
    
    function beforeFilter() {
        $this->pathPrefix  = WWW_ROOT . 'files' . DS . 'company_photos';
        $this->pathImage = $this->pathPrefix . DS;
        $this->urlImage = $this->webroot . 'files/company_photos/';
        $this->fieldImage = 'imagename';
        $this->resizedPrefix = 'thumb_';
        $this->thumbPrefix = 'thumbsmall_';
        parent::beforeFilter();
    }

    function index() {
        $this->__setAdditionals();
        $years = array();
        for ($i = 2005; $i < date('Y'); $i++) {
            $years[$i] = $i;
        }
        $this->set('years', $years);
        
        $condition = array();
        $records = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';

            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
        }
        $this->paginate['order'] = 'CompanyReport.name ASC';
        $records = $this->paginate($condition);
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            // link to map
            $records[$key]['CompanyReport']['actionLinks'] = '';
            if ( !empty($record['Company']['lat']) && !empty($record['Company']['lng']) ) {
                $records[$key]['CompanyReport']['actionLinks'] = $html->link('Lihat peta',
                                                         array(
                                                            'controller' => 'companies', 'action' => 'viewMap', 
                                                            $record['Company']['id']
                                                         ), array('class' => 'colorbox')
                                                         ) . ' &nbsp; ';
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__saving();
    }
    
    function edit($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        }
        
		$this->set('id', $id);
        $this->__setAdditionals($id);
        $this->__saving($id);
        
		if (empty($this->data)) {
			$this->data = $this->{$this->modelName}->find('first', array(
                'conditions' => array($this->modelName . '.id' => $id)
            ));
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->__redirect('index');
            }
		} else {
            $this->data[$this->modelName]['id'] = $id;
        }
    }

    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $company = $this->CompanyReport->find('first', array(
            'conditions' => array(
                'CompanyReport.id' => $id
            )
        ));
        if ( empty($company) ) {
            $this->Session->setFlash('Perusahaan tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set(compact('company'));
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->CompanyReport->id = $id;
        if ( $this->CompanyReport->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->CompanyReport->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->CompanyReport->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $companies = json_encode($this->CompanyReport->find('all'));
        $this->set(compact('companies'));
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
    }
    
    function mapClusterJson() {
        $this->layout = 'json/default';
        Configure::write('debug', 2);
        
        $companies = json_encode($this->CompanyReport->find('all'));
        $this->set(compact('companies'));
    }
    
    function __saving($id = null) {
        if ( !empty($this->data) ) {
			$messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __('successfully added', true);
			$messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                __('cannot add this new record. Please fix the errors mentioned belows', true);
            if ( $id ) {
                $messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("successcully edited", true);
                $messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                    __("cannot save this modification. Please fix the errors mentioned belows", true);
            }
            
            if ( $id ) {
                $this->CompanyReport->id = $id;
            } else {
                $this->CompanyReport->create();
            }
            $companies = $this->data;
            
			if ( $this->CompanyReport->save($this->data) ) {
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->redirect(array('action' => 'edit', $this->CompanyReport->id));
			} else {
                $this->set('companies', $companies['CompanyReport']);
                $this->Session->setFlash($messageFlashError, 'error');
			}
		}
    }
    
    function getCompany($id) {
        $this->layout = 'json/default';
        // first check on company report
        $report = $this->CompanyReport->find('first', array(
            'conditions' => array('company_id' => $id),
            'order' => 'report_date DESC',
            'recursive' => -1
        ));
        // then check on company if empty
        if ( empty($report) ) {
            $report = $this->CompanyReport->Company->find('first', array(
                'conditions' => array('id' => $id),
                'recursive' => -1
            ));
            $company = $report['Company'];
        } else {
            $company = $report['CompanyReport'];
        }
        
    }

    function __setAdditionals($id = null) {
        // set districts
        $districts = $this->CompanyReport->District->find('list', array(
            'order' => 'name', 'recursive' => 0
        ));
        $this->set('districts', $districts);
        
        $companies = $this->CompanyReport->Company->find('list', array(
            'order' => 'name', 'recursive' => 0
        ));
        $this->set('companies', $companies);
        
        // company types
        $this->set('company_types', array(
            'CV' => 'CV', 'PT' => 'PT', 'UD' => 'UD'
        ));
        
        // industry_categories
        $this->set('industry_categories', array(
            1 => 'Mesin, Logam dan Kimia', 2 => 'Aneka Industri'
        ));
        
        // unit_categories
        $this->set('unit_categories', array(
            1 => 'Kecil', 2 => 'Menengah', 3 => 'Besar'
        ));
        
        // units
        $this->set('units', $this->CompanyReport->CommodityCapacityUnit->find('list', array(
            'order' => 'name', 'recursive' => 0
        )));
        
        $province_id = null;
        $municipality_id = null;
        $district_id = null;
        
        $provinces = $__provinces = array();
        $municipalities = $__municipalities = array();
        $districts = $__districts = array();
        
        $_provinces = $this->CompanyReport->District->Municipality->Province->find('all', array(
            'order' => 'Province.name'
        ));
        $_districts = $this->CompanyReport->District->find('all', array(
            'fields' => array('id', 'name', 'municipality_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_municipalities = $this->CompanyReport->District->Municipality->find('all', array(
            'fields' => array('id', 'name', 'province_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_provinces = $this->CompanyReport->District->Municipality->Province->find('all', array(
            'fields' => array('id', 'name'), 'recursive' => -1,
            'order' => 'name'
        ));
        
        $max = max(count($_districts), count($_municipalities), count($_provinces));
        for ($i = 0; $i < $max; $i++) {
            if ( isset($_districts[$i]) ) {
                $districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District']['name'];
                $__districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District'];
            }
            if ( isset($_municipalities[$i]) ) {
                $municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality']['name'];
                $__municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality'];
            }
            if ( isset($_provinces[$i]) ) {
                $provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province']['name'];
                $__provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province'];
            }
        }
        if ( $id ) {
            $this->CompanyReport->Behaviors->attach('Containable');
            $company = $this->CompanyReport->find('first', array(
                'conditions' => array(
                    'CompanyReport.id' => $id
                ),
                'contain' => array()
            ));
            
            if ( !empty($id) ) {
                $district = $__districts[ $company['CompanyReport']['district_id'] ];
                $district_id = $district['id'];
                $municipality = $__municipalities[ $district['municipality_id'] ];
                $municipality_id = $municipality['id'];
                $province = $__provinces[ $municipality['province_id'] ];
                $province_id = $province['id'];
            }
            $this->set('company', $company);
        }

        $this->set('provinces', $provinces);
        $this->set('municipalities', $municipalities);
        $this->set('districts', $districts);
        
        $this->set('province_id', $province_id);
        $this->set('municipality_id', $municipality_id);
        $this->set('district_id', $district_id);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "';");
        $this->set('urlController', $this->__pathToController());
    }
}
?>
