<?php
class CompaniesController extends AppController {
    var $pageTitle = 'Data Perusahaan';
    
    // property related to upload image
    var $pathPrefix;
    var $pathImage;
    var $fieldImage;
    var $errorUpload;
    var $resizedPrefix;
    var $thumbPrefix;
    
    function beforeFilter() {
        $this->pathPrefix  = WWW_ROOT . 'files' . DS . 'company_photos';
        $this->pathImage = $this->pathPrefix . DS;
        $this->urlImage = $this->webroot . 'files/company_photos/';
        $this->fieldImage = 'imagename';
        $this->resizedPrefix = 'thumb_';
        $this->thumbPrefix = 'thumbsmall_';
        parent::beforeFilter();
    }

    function index() {
        $this->__setAdditionals();
        
        $condition = array();
        $records = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';

            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
        }
        $this->paginate['order'] = 'Company.name ASC';
        $records = $this->paginate($condition);
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            // link to map
            $records[$key]['Company']['actionLinks'] = '';
            if ( !empty($record['Company']['lat']) && !empty($record['Company']['lng']) ) {
                $records[$key]['Company']['actionLinks'] = $html->link('Lihat peta', array('action' => 'viewMap', 
                                                            $record['Company']['id']
                                                         ), array('class' => 'colorbox')
                                                         ) . ' &nbsp; ';
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__saving();
    }
    
    function edit($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        }
        
		$this->set('id', $id);
        $this->__setAdditionals($id);
        $this->__saving($id);
        
		if (empty($this->data)) {
			$this->data = $this->{$this->modelName}->find('first', array(
                'conditions' => array($this->modelName . '.id' => $id)
            ));
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->__redirect('index');
            }
		} else {
            $this->data[$this->modelName]['id'] = $id;
        }
    }
    
    function delImage($id, $company_id) {
        $this->Session->setFlash('Gambar tidak berhasil terhapus', 'error');
        
        $this->Company->CompanyImage->id = $id;
        $imagename = $this->Company->CompanyImage->field($this->fieldImage);
        if ( $this->Company->CompanyImage->delete($id) ) {
            @unlink($this->pathPrefix . DS . $imagename);
            @unlink($this->pathPrefix . DS . $this->resizedPrefix . $imagename);
            @unlink($this->pathPrefix . DS . $this->thumbPrefix . $imagename);
            $this->Session->setFlash('Gambar berhasil terhapus', 'success');
        }
        $this->redirect(array('action' => 'edit', $company_id));
    }
    
    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $company = $this->Company->find('first', array(
            'conditions' => array(
                'Company.id' => $id
            )
        ));
        if ( empty($company) ) {
            $this->Session->setFlash('Perusahaan tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set(compact('company'));
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->Company->id = $id;
        if ( $this->Company->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->Company->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->Company->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $companies = json_encode($this->Company->find('all'));
        $this->set(compact('companies'));
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
    }
    
    function mapClusterJson() {
        $this->layout = 'json/default';
        Configure::write('debug', 2);
        
        $companies = json_encode($this->Company->find('all'));
        $this->set(compact('companies'));
    }
    
    function __handleUploadImage($error_code, $tmp_name, $image_name) {
        $error = array();
        $possible_errors = array(
            1 => 'php.ini max file exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached'
        );
        
        // check for PHP's built-in uploading errors
        if ( $error_code > 0 ) {
            $error[] = $possible_errors[$error_code];
        }
        
        // check that the file we're working on really was the subject of an HTTP upload
        if ( !@is_uploaded_file($tmp_name) ) {
            $error[] = 'Not an HTTP upload';
        }
        
        // check if uploaded file is in fact an image
        if ( !@getimagesize($tmp_name) ) {
            $error[] = 'Only image uploads are allowed';
        }
        
        // check if our target directory is writeable
        if ( !is_writable($this->pathImage) ) {
            $error[] = 'Target upload is not writeable.';
        }
        
        if ( empty($error) ) {
            // Make a unique filename for the uploaded files and check it is not already
            // taken.
            
            // first check if we could use the original image name
            $image_path = $this->pathImage . $image_name;
            if ( file_exists($image_path) ) {
                $image_name = time() . '_' . $image_name;
                
                // uses time and iterate until it found unique name
                $image_path = $this->pathImage . $image_name;
                if ( file_exists($image_path) ) {
                    $counter = 1;
                    while ( file_exists($image_path = $this->pathImage . $counter . '_' . $image_name ) ) {
                        $counter++;
                    }
                    $image_name = $counter . '_' . $image_name;
                }
            }
            
            // error moving uploaded file
            if ( !@move_uploaded_file($tmp_name, $image_path) ) {
                $this->data['Relay'][$this->fieldImage] = '';
                $this->errorUpload = 'Receiving directory insufficient permission';
                
                return false;
            }
            
            // resizing
            $this->__resizeImage($image_name);
            
            return $image_name;
        } else {
            $this->errorUpload = $error;
            return false;
        }
    }
    
    function __saving($id = null) {
        if ( !empty($this->data) ) {
			$messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __('successfully added', true);
			$messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                __('cannot add this new record. Please fix the errors mentioned belows', true);
            if ( $id ) {
                $messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("successcully edited", true);
                $messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' .
                    __("cannot save this modification. Please fix the errors mentioned belows", true);
            }
            
            if ( $id ) {
                $this->Company->id = $id;
            } else {
                $this->Company->create();
            }
            $companies = $this->data;
            
			if ( $this->Company->save($this->data) ) {
                if ( isset($this->data['Company']['CompanyImage']['imagename']['name']) && 
                     !empty($this->data['Company']['CompanyImage']['imagename']['name']) ) {
                    $imgname = $this->data['Company']['CompanyImage']['imagename']['name'];
                    $tmps = $this->data['Company']['CompanyImage']['imagename']['tmp_name'];
                    $errors = $this->data['Company']['CompanyImage']['imagename']['error'];
                    
                    foreach ($imgname as $key => $name) {
                        if ( ($s = $this->__handleUploadImage($errors[$key], $tmps[$key], $name)) !== FALSE ) {
                            $saved_img = array(
                                'imagename' => $s, 'company_id' => $this->Company->id
                            );
                            $this->Company->CompanyImage->create();
                            $this->Company->CompanyImage->save($saved_img);
                        }
                    }
                }
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->redirect(array('action' => 'edit', $this->Company->id));
			} else {
                $this->set('companies', $companies['Company']);
                $this->Session->setFlash($messageFlashError, 'error');
			}
		}
    }

    function __setAdditionals($id = null) {
        // set districts
        $districts = $this->Company->District->find('list', array(
            'order' => 'name', 'recursive' => 0
        ));
        $this->set('districts', $districts);
        
        // company types
        $this->set('company_types', array(
            'CV' => 'CV', 'PT' => 'PT', 'UD' => 'UD'
        ));
        
        // industry_categories
        $this->set('industry_categories', array(
            1 => 'Mesin, Logam dan Kimia', 2 => 'Aneka Industri'
        ));
        
        // unit_categories
        $this->set('unit_categories', array(
            1 => 'Kecil', 2 => 'Menengah', 3 => 'Besar'
        ));
        
        // units
        $this->set('units', $this->Company->CommodityCapacityUnit->find('list', array(
            'order' => 'name', 'recursive' => 0
        )));
        
        $province_id = null;
        $municipality_id = null;
        $district_id = null;
        
        $provinces = $__provinces = array();
        $municipalities = $__municipalities = array();
        $districts = $__districts = array();
        
        $_provinces = $this->Company->District->Municipality->Province->find('all', array(
            'order' => 'Province.name'
        ));
        $_districts = $this->Company->District->find('all', array(
            'fields' => array('id', 'name', 'municipality_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_municipalities = $this->Company->District->Municipality->find('all', array(
            'fields' => array('id', 'name', 'province_id'), 'recursive' => -1,
            'order' => 'name'
        ));
        $_provinces = $this->Company->District->Municipality->Province->find('all', array(
            'fields' => array('id', 'name'), 'recursive' => -1,
            'order' => 'name'
        ));
        
        $max = max(count($_districts), count($_municipalities), count($_provinces));
        for ($i = 0; $i < $max; $i++) {
            if ( isset($_districts[$i]) ) {
                $districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District']['name'];
                $__districts[ $_districts[$i]['District']['id'] ] = $_districts[$i]['District'];
            }
            if ( isset($_municipalities[$i]) ) {
                $municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality']['name'];
                $__municipalities[ $_municipalities[$i]['Municipality']['id'] ] = $_municipalities[$i]['Municipality'];
            }
            if ( isset($_provinces[$i]) ) {
                $provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province']['name'];
                $__provinces[ $_provinces[$i]['Province']['id'] ] = $_provinces[$i]['Province'];
            }
        }
        if ( $id ) {
            $this->Company->Behaviors->attach('Containable');
            $company = $this->Company->find('first', array(
                'conditions' => array(
                    'Company.id' => $id
                ),
                'contain' => array(
                    'CompanyImage'
                )
            ));
            
            if ( !empty($id) ) {
                $district = $__districts[ $company['Company']['district_id'] ];
                $district_id = $district['id'];
                $municipality = $__municipalities[ $district['municipality_id'] ];
                $municipality_id = $municipality['id'];
                $province = $__provinces[ $municipality['province_id'] ];
                $province_id = $province['id'];
            }
            $this->set('company', $company);
        }

        $this->set('provinces', $provinces);
        $this->set('municipalities', $municipalities);
        $this->set('districts', $districts);
        
        $this->set('province_id', $province_id);
        $this->set('municipality_id', $municipality_id);
        $this->set('district_id', $district_id);
        
        // images related
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "';");
        $this->set('urlController', $this->__pathToController());
    }
}
?>
