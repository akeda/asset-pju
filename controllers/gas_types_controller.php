<?php
class GasTypesController extends AppController {
    var $pageTitle = 'Jenis Bahan Bakar';
    
    function index() {
        $this->paginate['order'] = 'GasType.name';
        parent::index();
    }
}
?>
