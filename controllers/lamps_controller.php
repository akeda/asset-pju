<?php
class LampsController extends AppController {
    var $pageTitle = 'Lampu';
    
    function index() {
        $this->__setAdditionals();
        
        $condition = array();
        $records = array();
        // filter show all in tablegrid
        if ( !$this->module_permission['show_all'] ) {
            $condition[$this->modelName . '.created_by'] = $this->Auth->user('id');
            if ( !empty($this->showAllExcept) ) {
                $isShowAll = array_intersect(array($this->Auth->user('group_id')), $this->showAllExcept);
                if ( !empty($isShowAll) ) {
                    unset($condition[$this->modelName . '.created_by']);
                }
            }
        }
        
        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Cari' ) {
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Cari';

            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
        }
        $this->paginate['order'] = 'Lamp.name ASC';
        $records = $this->paginate($condition);
        
        App::import('Helper', 'Html');
        $html = new HtmlHelper;
        foreach ($records as $key => $record) {
            // link to map
            $records[$key]['Lamp']['actionLinks'] = '';
            if ( !empty($record['Lamp']['lat']) && !empty($record['Lamp']['lng']) ) {
                $records[$key]['Lamp']['actionLinks'] = $html->link('Lihat peta', array('action' => 'viewMap', 
                                                            $record['Lamp']['id']
                                                         ), array('class' => 'colorbox')
                                                         );
            }
        }
        
        $this->set('str_fl', $str_fl);
        $this->set('records', $records);
        $this->set('formgrid', Helper::url('delete_rows'));
    }
    
    function excel() {
        $this->helpers[] = 'excel';
        $this->set('titleExcel', 'Exisiting');
        $this->set('templateExcel', 'lamps');
        
        $this->__setAdditionals();
        $condition = array();

        // filter by field passed in tablegrid
        $str_fl = '';
        if ( isset($this->params['url']['search']) && $this->params['url']['search'] == 'Print' ) {
            $this->layout = 'ajax';
            Configure::write('debug', 0);
            
            App::import('Sanitize');
            $filtered_link   = array();
            $filtered_link[] = '?search=Print';
            
            foreach ( $this->params['url'] as $param => $value ) {
                if ( !empty($value) && $param != 'url' && $param != 'search' && $param != 'ref') {
                    $param_name = substr($param, 4);
                    $param_type = substr($param, 0, 4);
                    $filtered_link[] = $param . "=" . $value;
                    $this->set($param_name . "Value", $value);
                    
                    if ( $param_type == 'opt_' ) {
                        $equal = Sanitize::paranoid($value);
                        $condition[$this->modelName.".$param_name"] = $equal;
                    } else if ( $param_type == 'txt_' ) {
                        $like = '%' . Sanitize::paranoid($value, array('*', ' ', '-', '.')) . '%';
                        $condition[$this->modelName.".$param_name LIKE"] = $like;
                    }
                }
            }
            
            if (!empty($filtered_link)) {
                $str_fl = implode("&", $filtered_link);
            }
            $this->set('str_fl', $str_fl);
            $data = $this->{$this->modelName}->find('all', array(
                'conditions' => $condition
            ));
            $this->set('data', $data);
        }
    }
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function add_bulk() {
        $this->__setAdditionals();
        
        if ( !empty($this->data) ) {
            $messageFlashSuccess = 'Data lampu berhasil ditambahkan semua';
            $messageFlashError   = 'Data lampu tidak berhasil ditambahkan semua';
            
            if ( isset($this->data[$this->modelName]['qty']) ) {
                
                $qty = $this->data[$this->modelName]['qty'];
                if (!is_numeric($qty) || $qty <= 0) {
                    $messageFlashError = 'Jumlah lampu harus benar';
                    $this->Session->setFlash($messageFlashError, 'error');
                    $this->redirect(array('action' => 'add_bulk'));
                }
                unset($this->data[$this->modelName]['qty']);
                
                // get count of lamps
                $count = $this->Lamp->find('count', array(
                    'conditions' => array(
                        'Lamp.relay_id' => $this->data[$this->modelName]['relay_id'],
                        'Lamp.panel_id' => $this->data[$this->modelName]['panel_id']
                    )
                ));
                
                $data = $this->data;
                $saved_data = array();
                $failed = false;
                for ($i = 1; $i <= $qty; $i++) {
                    $this->{$this->modelName}->create();
                    $this->{$this->modelName}->Subdistrict->Behaviors->attach('Containable');
                    $codes = $this->{$this->modelName}->Subdistrict->find('first', array(
                        'conditions' => array(
                            'Subdistrict.id' => $this->data['Lamp']['subdistrict_id']
                        ),
                        'fields' => array('code'),
                        'contain' => array(
                            'District' => array(
                                'fields' => array('code')
                            )
                        )
                    ));
                    $data[$this->modelName]['name'] = $codes['District']['code'] . '/' .
                                                      $codes['Subdistrict']['code'] . '/' .
                                                      $this->relays[$data[$this->modelName]['relay_id']] . '/' .
                                                      $this->panels[$data[$this->modelName]['panel_id']] . '/' .
                                                      str_pad($count+$i, 4, '0', STR_PAD_LEFT);
                    if (!$this->{$this->modelName}->save($data)) {
                        $failed = true;
                        break;
                    } else {
                        $saved_data[] = $this->{$this->modelName}->id;
                    }
                }
            } else {
                $this->Session->setFlash($messageFlashError, 'error');
                $this->redirect(array('action' => 'add_bulk'));
            }
            pr($this->{$this->modelName}->validationErrors);
            
            if (!$failed) {
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->__redirect();
            } else {
                $this->{$this->modelName}->delete($saved_data);
                $this->Session->setFlash($messageFlashError, 'error');
            }
		}
    }
    
    function edit($id) {
        $this->__setAdditionals();
        
        $lamp = $this->Lamp->find('first', array(
            'conditions' => array(
                'Lamp.id' => $id
            )
        ));
        if ( empty($lamp) ) {
            $this->Session->setFlash('Lampu tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set('lamp', $lamp);
        
        parent::edit($id);
    }
    
    function viewMap($id) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        $lamp = $this->Lamp->find('first', array(
            'conditions' => array(
                'Lamp.id' => $id
            )
        ));
        if ( empty($lamp) ) {
            $this->Session->setFlash('Lampu tidak ada', 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set(compact('lamp'));
    }
    
    function updateLatLng($id) {
        Configure::write('debug', 2);
        $this->layout = 'ajax';
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->Lamp->id = $id;
        if ( $this->Lamp->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        )) ) {
            $result = 1;
        }
       
        $this->set('result', $result);
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        $this->Lamp->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->Lamp->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function mapCluster() {
        $lamps = json_encode($this->Lamp->find('all'));
        $this->set(compact('lamps'));
    }
    
    function mapClusterJson() {
        $this->layout = 'json/default';
        Configure::write('debug', 2);
        
        $lamps = json_encode($this->Lamp->find('all'));
        $this->set(compact('lamps'));
    }
    
    function __setAdditionals() {
        $subdistricts = $this->Lamp->Subdistrict->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('subdistricts', $subdistricts);
        $this->subdistricts = $subdistricts;
        
        $streets = $this->Lamp->Street->find('list', array(
            'order' => 'name ASC'
        ));
        $this->set('streets', $streets);
        $this->streets = $streets;
        
        $types = $this->Lamp->LampType->find('list', array('order' => 'name ASC'));
        $this->set('types', $types);
        $this->types = $types;
        
        $panels = $this->Lamp->Panel->find('list', array('order' => 'name ASC'));
        $this->set('panels', $panels);
        $this->panels = $panels;
        
        $relays = $this->Lamp->Relay->find('list', array('order' => 'name ASC'));
        $this->set('relays', $relays);
        $this->relays = $relays;
        
        $pillar_types = $this->Lamp->PillarType->find('list', array('order' => 'name ASC'));
        $this->set('pillar_types', $pillar_types);
        
        $network_sizes = $this->Lamp->NetworkSize->find('list', array('order' => 'name ASC'));
        $this->set('network_sizes', $network_sizes);
        
        $network_types = $this->Lamp->NetworkType->find('list', array('order' => 'name ASC'));
        $this->set('network_types', $network_types);
        
        $this->set('urlController', $this->__pathToController());
    }
}
?>
