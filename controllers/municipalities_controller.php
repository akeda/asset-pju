<?php
class MunicipalitiesController extends AppController {
    var $pageTitle = 'Kotamadya / Kabupaten';
    
    function index() {
        $this->paginate['order'] = 'Municipality.name ASC';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        $this->__add(); 
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        parent::edit($id); 
    }
    
    function __setAdditionals() {
        $provinces = $this->Municipality->Province->find('list', array(
            'order' => 'Province.name'
        ));
        $this->set('provinces', $provinces);
    }
}
?>
