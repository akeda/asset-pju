<?php
/**
 * Excel template for panels
 * 
 * @author      gedex
 * @created     2010-11-19
 * @copyright   MIT license
 */
class panels_template {
    var $helper;
    var $xls;
    var $sheet;
    var $data;
    
    function __construct($helper, &$data) {
        $this->helper = $helper;
        $this->xls    = $this->helper->xls;
        $this->data   =& $data;
    }
    
    function title($title = '') {
    }
    
    function headers() {
    }
    
    function rows() {
        // start cell
        $sc = 'A';
        // end cell
        $ec = 'F';

        // style
        $border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
            'font' => array(
                'bold' => 1,
                'size' => 8
            )
        );
        $blue_bg = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
	 				'argb' => 'FF99CCFF'
	 			)
            )
        );
        $white_text = array(
            'font' => array(
                'color' => array('argb' => 'FFFFFFFF'),
                'bold' => 1,
                'size' => 8
            )
        );
        $reguler = array(
            'font' => array(
                'color' => array('argb' => 'FF000000'),
                'bold' => 0,
                'size' => 8
            )
        );
        $big = array(
            'font' => array(
                'color' => array('argb' => 'FF000000'),
                'bold' => 1,
                'size' => 22
            )
        );
        $center = array(
            'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
        );
        $left = array(
            'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
        );
        // headers
        $hds = array('No', 'Panel', 'Tipe', 'Kelurahan', 'Nama jalan', 'Gardu');
        $sheet_index = 0;
        $this->xls->setActiveSheetIndex($sheet_index);
        $this->sheet  = $this->xls->getActiveSheet();
        $this->sheet->setTitle( 'Gardu' );
        
        $c = $sc;
        $hd_style = array_merge($border, $center, $blue_bg);
        foreach ($hds as $k => $v) {
            $n = 0;
            $_c = $c;
            $this->sheet->setCellValue($c.'1', $v);
            $this->sheet->getColumnDimension($c)->setAutoSize(1);
            $this->sheet->getStyle($c.'1:'.$_c.'1')->applyFromArray($hd_style);
            $c = $this->helper->_nextCell($c, $n+1);
        }
        
        $row = 2;
        $no = 1;
        $row_style = $border;
        $types = array('I' => 'Induk', 'P' => 'Pembagi', 'N' => 'Natrium');
        
        foreach ($this->data as $key => $val) {
            // no
            $this->sheet->setCellValue($sc.$row, $no);
            $this->sheet->getStyle($sc.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($sc);
            
            // panel
            $this->sheet->setCellValue($c.$row, $val['Panel']['name']);
            $this->sheet->getStyle($c.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($c);
            
            // tipe
            $this->sheet->setCellValue($c.$row, $types[$val['Panel']['type']]);
            $this->sheet->getStyle($c.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($c);
            
            // kelurahan
            $this->sheet->setCellValue($c.$row, $val['Panel']['subdistrict']);
            $this->sheet->getStyle($c.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($c);
            
            // nama jalan
            $this->sheet->setCellValue($c.$row, $val['Street']['name']);
            $this->sheet->getStyle($c.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($c);
            
            // gardu
            $this->sheet->setCellValue($c.$row, $val['Relay']['name']);
            $this->sheet->getStyle($c.$row)->applyFromArray($row_style);
            $c = $this->helper->_nextCell($c);
            
            $row++;
            $no++;
        }
        $this->xls->setActiveSheetIndex(0);
    }
}
